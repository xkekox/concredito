<div class="modal fade out MessageError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <center class="text-danger" id="message_error"></center>
            </div>
        </div>
    </div>
</div>