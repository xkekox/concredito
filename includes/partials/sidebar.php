<nav id="myNavmenu" class="navmenu navmenu-default navmenu-fixed-left offcanvas" role="navigation" style="background-color: #515151; margin-top: 0px;">
    <a class="navmenu-brand text-center" href="#" style="background-color: rgba(0,0,0,0.4); margin-bottom: 0px; margin-top:0px; color:white; border-bottom: 1px solid #cccccc; padding-top:15px; padding-bottom: 15px;">AG College</a>
    <ul class="nav navmenu-nav">
        <li style="border-bottom: 1px solid white;"><a style="color:white; border-left:2px solid green" href="/directorio"><i class="fa fa-home fa-lg" aria-hidden="true"></i> Inicio</a></li>
        <li style="border-bottom: 1px solid white;"><a style="color:white; border-left:2px solid #3498db" href="/directorio/tareas/"><i class="fa fa-bullseye fa-lg" aria-hidden="true"></i> Actividades</a></li>
        <li style="border-bottom: 1px solid white;"><a style="color:white; border-left:2px solid red" href="/directorio/personal/"><i class="fa fa-address-book-o fa-lg" aria-hidden="true"></i> Personal</a></li>
        <li style="border-bottom: 1px solid white;"><a style="color:white; border-left:2px solid orange" href="/directorio/sistemas/"><i class="fa fa fa-file-text-o fa-lg" aria-hidden="true"></i> Sistemas</a></li>
        <li style="border-bottom: 1px solid white;"><a style="color:white; border-left:2px solid blue" href="/directorio/respaldos/"><i class="fa fa-database fa-lg" aria-hidden="true"></i> Respaldos</a></li>
        <li style="border-bottom: 1px solid white;"><a style="color:white; border-left:2px solid yellow" href="/directorio/eventos/"><i class="fa fa-calendar fa-lg" aria-hidden="true"></i> Fechas Importantes</a></li>
        <!--
        <li><a href="/directorio/javier/">JAVIER</a></li>
        <li><a href="/directorio/cisnes/">CISNES</a></li>
         -->

    </ul>

    <?php

    //echo '<li><a href="/directorio/sistemas/"><i class="fa fa fa-file-text-o fa-2x" aria-hidden="true"></i><br/> Sistemas</a></li>';
    //if($_SESSION['permiso'] == 1){
    //    if($_SESSION['area'] == 4){
    //        echo '<li><a href="/directorio/respaldos/"><i class="fa fa-database fa-2x" aria-hidden="true"></i><br/> Respaldos</a></li>';
    //    }
    //}
    //
    ?>
</nav>