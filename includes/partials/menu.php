<div class="navbar navbar-default navbar-fixed-top navbar-inverse" >
    <div class="container-fluid" style="padding-left: 0px;">

        <div class="navbar-header">




            <div class="visible-xs" style="padding-top: 15px;  padding-left: 30px;  ">



                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color:white; text-decoration: none;">Inicio <span class="caret"></span></a>
                <ul class="dropdown-menu">

                    <li><a href="/vendimia/ventas/" style="display: block; overflow: auto;"><i class="fa fa-usd" aria-hidden="true"></i> Ventas</a></li>
                    <li role="separator" class="divider"></li>

                    <li><a href="/vendimia/clientes/" style="display: block; overflow: auto;"><li><i class="fa fa-user" aria-hidden="true"></i> Clientes</a></li>
                    <li><a href="/vendimia/articulos/" style="display: block; overflow: auto;"><i class="fa fa-tags" aria-hidden="true"></i> Articulos</a></li>
                    <li><a href="/vendimia/configuracion/create" style="display: block; overflow: auto;"><i class="fa fa-cog" aria-hidden="true"></i></i> Configuración </a></li>


                    <li role="separator" class="divider"></li>
                    <li><a href="#" style="display: block; overflow: auto;" class="logout"><span class="label label-danger pull-right"></span><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
                </ul>
                <a style="color:white; margin-right: 10px;" class="pull-right">Fecha: <?php echo date('j/m/y'); ?> </a>

            </div>


        </div>

        <p class="navbar-text navbar-right hidden-xs" style="color:white; margin-right: 20px;">Fecha: <?php echo date('j/m/y'); ?> </p>

        <ul class="nav navbar-nav navbar-left hidden-xs">


            <li class="dropdown" style="border-left: 1px solid #e7e7e7;border-right: 1px solid #e7e7e7; ">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Inicio <span class="caret"></span></a>
                <ul class="dropdown-menu">

                    <li><a href="/vendimia/ventas/" style="display: block; overflow: auto;"><i class="fa fa-usd" aria-hidden="true"></i> Ventas</a></li>
                    <li role="separator" class="divider"></li>

                    <li><a href="/vendimia/clientes/" style="display: block; overflow: auto;"><li><i class="fa fa-user" aria-hidden="true"></i> Clientes</a></li>
                    <li><a href="/vendimia/articulos/" style="display: block; overflow: auto;"><i class="fa fa-tags" aria-hidden="true"></i> Articulos</a></li>
                    <li><a href="/vendimia/configuracion/create" style="display: block; overflow: auto;"><i class="fa fa-cog" aria-hidden="true"></i></i> Configuración </a></li>


                    <li role="separator" class="divider"></li>
                    <li><a href="#" style="display: block; overflow: auto;" class="logout"><span class="label label-danger pull-right"></span><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
                </ul>
            </li>
        </ul>



    </div>
</div>




