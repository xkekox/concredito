<!-- Scripts Generales -->
<!-- <script src="assets/js/libs/angular.js"></script>
<script src="assets/js/libs/angular-route.js"></script> -->
<script src="assets/js/libs/jquery.js"></script>
<script src="assets/js/libs/bootstrap.js"></script>
<script src="assets/js/libs/jasny-bootstrap.js"></script>
<script src="assets/js/libs/formValidation.js"></script>
<script src="assets/js/framework/bootstrap.min.js"></script>
<script src="assets/js/libs/jquery.dataTables.js"></script>
<script src="assets/js/libs/dataTables.responsive.min.js"></script>

<script src="assets/js/libs/toastr.js"></script>
<script src="controllers/global.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries

-->
<!--[if gt IE 7]>
<script src="assets/js/libs/html5shiv.js"></script>
<script src="assets/js/libs/respond.js"></script>
<![endif]-->

