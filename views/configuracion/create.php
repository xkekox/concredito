<?php
session_start();
ob_start();
//print_r($_SESSION['session']);
if($_SESSION['user_id'] != null){
    //header("location: dashboard.php");
}else{
    header("location: login");
}

include_once('../../back/Configuracion.php');
$Configuracion = new Configuracion();
$get_configuracion = $Configuracion->show();


if($get_configuracion->success){
    $tasa = isset($get_configuracion->data[0]->tasa_financiamiento) ? $get_configuracion->data[0]->tasa_financiamiento : '';
    $porcentaje = isset($get_configuracion->data[0]->porcentaje_enganche) ? $get_configuracion->data[0]->porcentaje_enganche : '';
    $plazo = isset($get_configuracion->data[0]->plazo_maximo) ? $get_configuracion->data[0]->plazo_maximo : '';
}else{
    $tasa = '';
    $porcentaje ='';
    $plazo = '';
}





?>

<!doctype html>
<html lang="en">
<head>
    <?php include('../../includes/partials/styles_sub1.html') ?>
    <link rel="stylesheet" href="../assets/css/configuracion.css"/>
</head>
<body id="configuracion_create">

<!--  Navbar -->
<?php include('../../includes/partials/menu.php'); ?>



<!-- Contenido Princiapl -->
<section>
    <div class="main_wrapper">

        <div class="container-fluid">
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="/vendimia/">Inicio</a></li>
                    <li class="active">Configuración</li>

                </ol>
            </div>
        </div>


        <div class="container-fluid">




            <!-- Main title -->
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <section class="main-title">

                        <h1 align="left" style="border-left: 4px solid #83b35b; padding-left: 10px;">Configuración General</h1>


                    </section>
                </div>
            </div>

            <!-- Formulario -->
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1" >


                    <form id="form_configuracion_add" name="frmConfiguracionAdd" method="post" class="form-horizontal">


                        <div class="panel panel-default">
                            <div class="panel-body">
                                <br/>
                                <center><span class="text-center display-errors show_errors"></span></center>


                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Tasa Financiamiento</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="configuracion_tasa" placeholder="Ingresa la tasa de financiamiento..." value="<?php echo $tasa ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">% de Enganche</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="configuracion_enganche" placeholder="Ingresa el % de enganche..." value="<?php echo $porcentaje ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Plazo Máximo</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="configuracion_plazo" placeholder="Ingresa el plazo máximo..." value="<?php echo $plazo ?>">
                                    </div>
                                </div>



                                <input type="hidden" name="userid" value="'<?php echo $_SESSION['user_id'] ?>'"/>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row text-center">

                                <div class="col-sm-9">

                                </div>
                                <div class="col-sm-3">
                                    <button type="button" class="btn btn-default btn-confirm" >Cancelar</button>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" name="submitButton" class="btn btn-default" >Guardar</button>

                                </div>

                            </div>
                        </div>



                    </form>
                </div>
            </div>



        </div>

    </div>
</section>


<!-- Messages -->
<section id="messages_inicio">
    <?php
    include('../../includes/messages/success.php');
    include('../../includes/messages/error.php');
    include('../../includes/messages/warning.php');
    ?>
</section>


<!-- Scripts Generales -->
<section class="scripts">
    <?php include('../../includes/partials/scripts_sub1.php') ?>
    <script src="../controllers/configuracion.js"></script>
    <script src="../controllers/formsValidation/configuracion_form_validation.js"></script>

</section>

</body>
</html>