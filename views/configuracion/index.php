<?php
session_start();
ob_start();
//print_r($_SESSION['session']);
if($_SESSION['user_id'] != null){
    //header("location: dashboard.php");
}else{
    header("location: login");
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>AG College - Directorio</title>
    <?php include('../../includes/partials/styles_sub1.html') ?>
    <link rel="stylesheet" href="../assets/css/styles.css"/>
</head>

<body style="background-color: white/*#ecf0f1*/; padding-bottom: 50px; padding-top: 20px;">

<!--  Navbar -->
<?php include('../../includes/partials/menu.php'); ?>





<!-- Contenido Princiapl -->
<section>
    <div class="main_wrapper">
        <br/>

        <div class="container-fluid">

            <!-- Main title -->

            <section class="main_title">
                <div class="row">
                    <h1 align="center" class="montserrat" style="margin-bottom: 0px;">Colaboradores</h1>
                    <hr style="margin-top: 0px; margin-bottom: 10px; width: 10%; border-top:1px solid #e7e7e7;" />
                    <br/>
                </div>
            </section>


            <section class="agenda_wrapper">
                <div class="row">
                    <div class="col-sm-12">


                    </div>
                </div>
            </section>



        </div>


    </div>
</section>


<!-- Messages -->
<section id="messages_inicio">
    <?php
    include('../../includes/messages/success.php');
    include('../../includes/messages/error.php');
    include('../../includes/messages/warning.php');
    ?>
</section>


<!-- Scripts Generales -->
<section class="scripts">
    <?php include('../../includes/partials/scripts_sub1.php') ?>
    <script src="../controllers/agenda.js"></script>

</section>

</body>
</html>