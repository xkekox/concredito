<?php
session_start();
ob_start();
//print_r($_SESSION['session']);
if($_SESSION['user_id'] != null){
    //header("location: dashboard.php");
}else{
    header("location: login");
}

include_once('../../back/Cliente.php');
$Cliente = new Cliente();
$boletas = $Cliente->index();

?>

<!doctype html>
<html lang="en">
<head>
    <?php include('../../includes/partials/styles_sub1.html') ?>
    <link rel="stylesheet" href="../assets/css/clientes.css"/>
</head>
<body id="clientes_index">

<!--  Navbar -->
<?php include('../../includes/partials/menu.php'); ?>



<!-- Contenido Principal -->
<section>
    <div class="main_wrapper">

        <div class="container-fluid" >
            <!-- Breadcrumb -->
            <div class="row">
                <ol class="breadcrumb" >
                    <li><a href="/vendimia/">Inicio</a></li>
                    <li>Clientes</li>
                    <span class="option_add pull-right" ><a href="/vendimia/clientes/create">Nuevo Cliente <i class="fa fa-plus" aria-hidden="true"></i></a></span>
                </ol>


            </div>


        </div>

        <div class="container-fluid">




            <!-- Main title -->
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <section class="main-title">

                        <h1 align="left">Clientes Registrados</h1>
                        <hr align="left" style="margin-top: 0px; margin-bottom: 10px; width: 100%; border-top:3px solid #83b35b;" />
                        <br/>

                    </section>
                </div>
            </div>


            <!-- Table information -->
            <section class="clientes_wrapper">

                <div class="clearfix"></div>

                <div class="row" >
                    <div class="col-sm-10 col-sm-offset-1" >

                        <section class="table_information_clientes" >

                            <div class="row">





                                    <section class="">
                                        <table id="table_clientes_list" class="table table-bordered dt-responsive table_sistemas_list" cellspacing="0">
                                            <thead>
                                            <tr>
                                                <th class="text-center" style="vertical-align: middle;" width="20%">Clave Cliente</th>
                                                <th class="text-left" style="vertical-align: middle;" width="70%">Nombre</th>
                                                <th class="text-left" style="vertical-align: middle;" width="10%">Consultar</th>



                                            </tr>
                                            </thead>
                                            <tbody class="table_info_clientes">
                                                    <?php
                                                        $code_table = '';


                                                    if(count(@$boletas->data) > 0){
                                                        foreach($boletas->data as $data){
                                                            $code_table .='
                                                                    <tr>
                                                                      <td align="center" class="text-center" style="vertical-align: middle; " scope="row">';

                                                            $num =$data->id;

                                                            if($num < 10){
                                                                $code_table .= '000'.$num;
                                                            }else if($num < 100){
                                                                $code_table .= '00'.$num;
                                                            }else if($num < 1000){
                                                                $code_table .= '0'.$num;
                                                            }
                                                            $code_table .='</td>
                                                                      <td align="left" class="text-left" style="vertical-align: middle; ">'.$data->nombre.' '.$data->apellido_paterno.' '.$data->apellido_materno.'</td>

                                                                      <td align="center" class="text-center" style="vertical-align: middle; ">
                                                                        <a href="/vendimia/clientes/'. $data->id.'"><button type="button" class="btn btn_consultar" ><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                                                      </td>
                                                                    </tr>
                                                                    ';
                                                        }
                                                        echo $code_table;
                                                    }



                                                    ?>
                                            </tbody>
                                        </table>

                                    </section>





                            </div>
                            <br/>
                        </section>
                    </div>
                </div>
            </section>



        </div>

    </div>
</section>


<!-- Messages -->
<section id="messages_inicio">
    <?php
    include('../../includes/messages/success.php');
    include('../../includes/messages/error.php');
    include('../../includes/messages/warning.php');
    ?>
</section>


<!-- Scripts Generales -->
<section class="scripts">
    <?php include('../../includes/partials/scripts_sub1.php') ?>
    <script src="../controllers/clientes.js"></script>

</section>

</body>
</html>