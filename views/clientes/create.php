<?php
session_start();
ob_start();
//print_r($_SESSION['session']);
if($_SESSION['user_id'] != null){
    //header("location: dashboard.php");
}else{
    header("location: login");
}

include_once('../../back/Cliente.php');
$Cliente = new Cliente();
$boletas = $Cliente->get_num_boleta();



?>

<!doctype html>
<html lang="en">
<head>
    <?php include('../../includes/partials/styles_sub1.html') ?>
    <link rel="stylesheet" href="../assets/css/clientes.css"/>
</head>
<body id="clientes_create">

<!--  Navbar -->
<?php include('../../includes/partials/menu.php'); ?>



<!-- Contenido Princiapl -->
<section>
    <div class="main_wrapper">

        <div class="container-fluid" >
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="/vendimia/">Inicio</a></li>
                    <li><a href="/vendimia/clientes/">Clientes</a></li>
                    <li class="active">Agregar Cliente</li>
                </ol>
            </div>
        </div>


        <div class="container-fluid">




            <!-- Main title -->
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <section class="main-title">

                        <h1 align="left" style="border-left: 4px solid #83b35b; padding-left: 10px;">Registro de clientes</h1>


                    </section>
                </div>
            </div>

            <!-- Formulario -->
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1" >


                    <form id="form_cliente_add" name="frmClienteAdd" method="post" class="form-horizontal">


                        <div class="panel panel-default">
                            <div class="panel-body">

                                    <center><span class="text-center display-errors show_errors"></span></center>



                                    <p class="text-right">Clave: <span style="text-decoration: underline;">
                                        <?php

                                        if(!isset($boletas->data[0]->id)){
                                            $num =1;
                                        }else{
                                            $num =$boletas->data[0]->id+1;
                                        }

                                        if($num < 10){
                                            echo '000'.$num;
                                        }else if($num < 100){
                                            echo '00'.$num;
                                        }else if($num < 1000){
                                            echo '0'.$num;
                                        }



                                        ?></span></p>
                                    <br/>


                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Nombre</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="cliente_nombre" placeholder="Ingresa tu nombre(s)...">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Apellido Paterno</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="cliente_apellido_paterno" placeholder="Ingresa tu apellido paterno...">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Apellido Materno</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="cliente_apellido_materno" placeholder="Ingresa tu apellido materno...">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">RFC</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="cliente_rfc" placeholder="Ingresa tu RFC...">
                                        </div>
                                    </div>

                                <input type="hidden" name="userid" value="'<?php echo $_SESSION['user_id'] ?>'"/>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">

                                <div class="col-sm-9">

                                </div>
                                <div class="col-sm-3">
                                    <button type="button" class="btn btn-default btn-confirm" >Cancelar</button>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" name="submitButton" class="btn btn-default" >Guardar</button>

                                </div>

                            </div>
                        </div>



                    </form>
                </div>
            </div>



        </div>

    </div>
</section>


<!-- Messages -->
<section id="messages_inicio">
    <?php
    include('../../includes/messages/success.php');
    include('../../includes/messages/error.php');
    include('../../includes/messages/warning.php');
    ?>
</section>


<!-- Scripts Generales -->
<section class="scripts">
    <?php include('../../includes/partials/scripts_sub1.php') ?>
    <script src="../controllers/clientes.js"></script>
    <script src="../controllers/formsValidation/cliente_form_validation.js"></script>

</section>

</body>
</html>