<?php
session_start();
ob_start();
//print_r($_SESSION['session']);
if($_SESSION['user_id'] != null){
    //header("location: dashboard.php");
}else{
    header("location: login");
}

include_once('../../back/Venta.php');
$venta = new Ventas();


?>

<!doctype html>
<html lang="en">
<head>
    <?php include('../../includes/partials/styles_sub1.html') ?>
    <link rel="stylesheet" href="../assets/css/ventas.css"/>
</head>
<style>

    .auto_clientes ul{
        background-color:#fff;
        padding: 5px 5px;
        cursor:pointer;
    }
    .auto_clientes li{
        padding:0px ;
    }

    .auto_articulos ul{
        background-color:#fff;
        padding: 5px 5px;
        cursor:pointer;
    }
    .auto_articulos li{
        padding:0px;
    }
</style>

<body id="ventas_create">

<!--  Navbar -->
<?php include('../../includes/partials/menu.php'); ?>



<!-- Contenido Princiapl -->
<section>
    <div class="main_wrapper">

        <div class="container-fluid">
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="/vendimia/">Inicio</a></li>
                    <li><a href="/vendimia/ventas/">Ventas</a></li>
                    <li class="active">Registro Venta</li>

                </ol>
            </div>
        </div>


        <div class="container-fluid">




            <!-- Main title -->
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <section class="main-title">

                        <h1 align="left" style="border-left: 4px solid #83b35b; padding-left: 10px;">Registro de Ventas</h1>


                    </section>
                </div>
            </div>

            <!-- Panel de informacion -->
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-body">


                            <!-- Autocompletes -->
                            <section>
                                <!-- Clientes -->
                                    <div class="row text-center">
                                        <span class="text-danger text_error_general"></span>
                                    </div>
                                    <div class="row">

                                        <?php
                                        $num_boleta_venta =$venta->boleta_venta_actual();
                                        $boleta_venta = ($num_boleta_venta == 0) ? '1' : $num_boleta_venta+1;
                                        echo '<span class="pull-right" style="padding-right: 30px;">000'.$boleta_venta.'</span>';


                                        ?>
                                        <input type="hidden" value="<?php echo $boleta_venta?>" class="venta_boleta_num"/>


                                        <div class="col-sm-6">

                                            <div class="form-group">

                                                <div class="auto_clientes">
                                                    <label>Cliente</label>
                                                    <input type="text" name="autocomplete_clientes" id="autocomplete_clientes" class="form-control autocomplete_clientes" placeholder="Buscar cliente..." />
                                                    <span class="text_error_cliente text-danger"></span>
                                                    <div id="clientesList" class="text-center" style="border:1px solid #ececec; display:none;"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <span style=" display: none;" id="display_rfc"></span>
                                        </div>
                                    </div>

                                    <hr align="left"  style="margin-top: 0px; margin-bottom: 10px; width: 50%; border-top:1px solid #ececec;"/>

                                    <!-- Articulos -->
                                    <div class="row autocomplete_clientes_section">
                                        <div class="col-sm-6">
                                            <div class="form-group">

                                                <div class="auto_clientes">
                                                    <label>Articulo</label>
                                                    <input type="text" name="autocomplete_articulos" id="autocomplete_articulos" class="form-control autocomplete_articulos" placeholder="Buscar articulo..." />
                                                    <span class="text_error_articulo text-danger"></span>
                                                    <div id="articulosList" class="text-center" style="border:1px solid #ececec; display:none; padding 5px 5px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <p style="padding-top: 25px;"><button type="button" class="btn btn-success btn_agregar_articulo" style="border-radius: 0px; cursor: pointer;" ><i class="fa fa-plus" aria-hidden="true"></i></button></p>
                                        </div>
                                    </div>

                            </section>


                            <!-- Table informacion productos -->
                            <section class="wrapper_table_info" style="display: none;">
                                <hr align="left" style="margin-top: 10px; margin-bottom: 0px; width: 100%; border-top:2px solid #333;"/>
                                <table id="table_clientes_list" class="table table-bordered dt-responsive table_sistemas_list" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th class="text-left" style="vertical-align: middle;" width="35%">Descripción Articulo</th>
                                        <th class="text-left" style="vertical-align: middle;" width="25%">Modelo</th>
                                        <th class="text-center" style="vertical-align: middle;" width="5%">Cantidad</th>
                                        <th class="text-center" style="vertical-align: middle;" width="15%">Precio</th>
                                        <th class="text-center" style="vertical-align: middle;" width="15%">Importe</th>
                                        <th class="text-center" style="vertical-align: middle;" width="5%">&nbsp;</th>



                                    </tr>
                                    </thead>
                                    <tbody id="table_ventas_articulos" class="table_ventas_articulos">

                                    </tbody>
                                </table>

                                <br/>

                                <hr align="left" style="margin-top: 0px; margin-bottom: 10px; width: 100%; border-top:2px solid #333;"/>
                                <section class="ventas_formular">
                                    <div class="row">
                                        <div class="col-sm-7">

                                        </div>
                                        <div class="col-sm-4 text-right">
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <p>Enganche: </p>
                                                </div>
                                                <div class="col-sm-3">
                                                    <span id="venta_enganche"></span>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-sm-1">

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-7">

                                        </div>
                                        <div class="col-sm-4 text-right">
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <p>Bonificación Enganche: </p>
                                                </div>
                                                <div class="col-sm-3">
                                                    <span id="venta_bonificacion"></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-1">

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-7">

                                        </div>
                                        <div class="col-sm-4 text-right">
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <p>Total: </p>
                                                </div>
                                                <div class="col-sm-3">
                                                    <span id="venta_adeudo"></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-1">

                                        </div>
                                    </div>
                                </section>

                                <br/>
                                <section class="abonos_mensuales" style="display: none;">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table class="table table-condensed">
                                                <thead>
                                                <tr>
                                                    <th align="center" class="text-center" colspan="4">ABONOS MENSUALES</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td width="20%">3 ABONOS DE </td>
                                                    <td width="20%"><span class="importe_abono_3"> </span></td>
                                                    <td width="30%"><span class="total_pago_3"> </span></td>
                                                    <td width="30%"><span class="ahorro_3"></span></td>
                                                    <td>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="abono_mensual" id="abono_mensual_3" value="3" >

                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">6 ABONOS DE </td>
                                                    <td width="20%"><span class="importe_abono_6"> </span></td>
                                                    <td width="30%"><span class="total_pago_6"> </span></td>
                                                    <td width="30%"><span class="ahorro_6"></span></td>
                                                    <td>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="abono_mensual" id="abono_mensual_6" value="6" >

                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">9 ABONOS DE </td>
                                                    <td width="20%"><span class="importe_abono_9"> </span></td>
                                                    <td width="30%"><span class="total_pago_9"> </span></td>
                                                    <td width="30%"><span class="ahorro_9"></span></td>
                                                    <td>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="abono_mensual" id="abono_mensual_9" value="9" >

                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">12 ABONOS DE </td>
                                                    <td width="20%"><span class="importe_abono_12"> </span></td>
                                                    <td width="30%"><span class="total_pago_12"> </span></td>
                                                    <td width="30%"><span class="ahorro_12"></span></td>
                                                    <td>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="abono_mensual" id="abono_mensual_12" value="12" >

                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <!-- Tool by http://unminify-tool.blogspot.com/ -->
                                        </div>



                                    </div>
                                </section>

                            </section>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-5">

                </div>
                <div class="col-sm-4">

                </div>
                <div class="col-sm-3 text-center">
                    <div style="padding-right: 15px;">
                        <button type="button" class="btn btn-success btn_step_2" style="display: none;">Siguiente</button>
                    </div>

                    <div style="padding-right: 90px; display: none;" class="last_step" >
                        <button type="button" class="btn btn-success btn-confirm" >Cancelar</button>
                        <button type="button" class="btn btn-success btn_last_step">Guardar</button>
                    </div>

                </div>


            </div>












        </div>

    </div>
</section>


<!-- Messages -->
<section id="messages_inicio">
    <?php
    include('../../includes/messages/success.php');
    include('../../includes/messages/error.php');
    include('../../includes/messages/warning.php');
    ?>
</section>


<!-- Scripts Generales -->
<section class="scripts">
    <?php include('../../includes/partials/scripts_sub1.php') ?>
    <script src="../controllers/ventas.js"></script>

</section>

</body>
</html>