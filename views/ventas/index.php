<?php
session_start();
ob_start();
//print_r($_SESSION['session']);
if($_SESSION['user_id'] != null){
    //header("location: dashboard.php");
}else{
    header("location: login");
}


include_once('../../back/Venta.php');
$ventas = new Ventas();
$ventas = $ventas->index();

?>

<!doctype html>
<html lang="en">
<head>
    <?php include('../../includes/partials/styles_sub1.html') ?>
    <link rel="stylesheet" href="../assets/css/ventas.css"/>
</head>
<body id="ventas_index">

<!--  Navbar -->
<?php include('../../includes/partials/menu.php'); ?>



<!-- Contenido Principal -->
<section>
    <div class="main_wrapper">

        <div class="container-fluid" >
            <!-- Breadcrumb -->
            <div class="row">
                <ol class="breadcrumb" >
                    <li><a href="/vendimia/">Inicio</a></li>
                    <li>Ventas</li>
                    <span class="option_add pull-right" ><a href="/vendimia/ventas/create">Nueva Venta <i class="fa fa-plus" aria-hidden="true"></i></a></span>
                </ol>


            </div>


        </div>

        <div class="container-fluid">




            <!-- Main title -->
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <section class="main-title">

                        <h1 align="left">Ventas Registradas</h1>
                        <hr align="left" style="margin-top: 0px; margin-bottom: 10px; width: 100%; border-top:3px solid #83b35b;" />
                        <br/>

                    </section>
                </div>
            </div>


            <!-- Table information -->
            <section class="ventas_wrapper">

                <div class="clearfix"></div>

                <div class="row" >
                    <div class="col-sm-10 col-sm-offset-1" >

                        <section class="table_information_ventas" >

                            <div class="row">





                                <section class="">
                                    <table id="table_ventas_list" class="table table-bordered dt-responsive table_ventas_list" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th class="text-center" style="vertical-align: middle;" width="10%">Folio Venta</th>
                                            <th class="text-center" style="vertical-align: middle;" width="15%">Clave Cliente</th>
                                            <th class="text-left" style="vertical-align: middle;" width="35%">Nombre</th>
                                            <th class="text-left" style="vertical-align: middle;" width="15%">Total</th>
                                            <th class="text-left" style="vertical-align: middle;" width="15%">Fecha</th>



                                        </tr>
                                        </thead>
                                        <tbody class="table_info_ventas">
                                        <?php
                                        $code_table = '';

                                        if(count(@$ventas->data) > 0){
                                            foreach($ventas->data as $data){

                                                $code_table .='
                                                                    <tr>
                                                                      <td align="center" class="text-center" style="vertical-align: middle; " scope="row">';

                                                $num_folio =$data->folio;

                                                if($num_folio < 10){
                                                    $code_table .= '000'.$num_folio;
                                                }else if($num_folio < 100){
                                                    $code_table .= '00'.$num_folio;
                                                }else if($num_folio < 1000){
                                                    $code_table .= '0'.$num_folio;
                                                }

                                                $code_table .='</td>
                                                                      <td align="center" class="text-center" style="vertical-align: middle; ">  ';
                                                $num_cliente =$data->cliente;

                                                if($num_cliente < 10){
                                                    $code_table .= '000'.$num_cliente;
                                                }else if($num_cliente < 100){
                                                    $code_table .= '00'.$num_cliente;
                                                }else if($num_cliente < 1000){
                                                    $code_table .= '0'.$num_cliente;
                                                }
                                                $code_table .=' </td>


                                                                      <td align="left" class="text-left" style="vertical-align: middle; ">'.$data->nombre.'</td>
                                                                      <td align="left" class="text-left" style="vertical-align: middle; ">'.$data->total.'</td>
                                                                      <td align="left" class="text-left" style="vertical-align: middle; ">'.date("d/m/Y",strtotime($data->fecha)).'</td>

                                                                    </tr>
                                                                    ';
                                            }
                                            echo $code_table;
                                        }


                                        ?>
                                        </tbody>
                                    </table>

                                </section>





                            </div>
                            <br/>
                        </section>
                    </div>
                </div>
            </section>



        </div>

    </div>
</section>


<!-- Messages -->
<section id="messages_inicio">
    <?php
    include('../../includes/messages/success.php');
    include('../../includes/messages/error.php');
    include('../../includes/messages/warning.php');
    ?>
</section>


<!-- Scripts Generales -->
<section class="scripts">
    <?php include('../../includes/partials/scripts_sub1.php') ?>
    <script src="../controllers/ventas.js"></script>

</section>

</body>
</html>