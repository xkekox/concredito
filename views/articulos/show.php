<?php
session_start();
ob_start();
//print_r($_SESSION['session']);
if($_SESSION['user_id'] != null){
    //header("location: dashboard.php");
}else{
    header("location: login");
}

$articulo_id= $_REQUEST["id"];
include_once('../../back/Articulo.php');
$Articulo = new Articulo();
$info_articulo = $Articulo->show($articulo_id);
if(!isset($info_articulo->data[0])){
    header("location: /vendimia/articulos/");
}
?>

<!doctype html>
<html lang="en">
<head>
    <?php include('../../includes/partials/styles_sub1.html') ?>
    <link rel="stylesheet" href="../assets/css/articulos.css"/>
</head>
<body id="articulos_show">

<!--  Navbar -->
<?php include('../../includes/partials/menu.php'); ?>



<!-- Contenido Princiapl -->
<section>
    <div class="main_wrapper">

        <div class="container-fluid">
            <!-- Breadcrumb -->
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="/vendimia/">Inicio</a></li>
                    <li><a href="/vendimia/articulos/">Articulos</a></li>
                    <li class="active">Ver Articulo</li>
                </ol>


            </div>


        </div>

        <div class="container-fluid">




            <!-- Main title -->
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <section class="main-title">

                        <h1 align="left">Información del Articulo</h1>
                        <hr align="left" style="margin-top: 0px; margin-bottom: 10px; width: 100%; border-top:3px solid #83b35b;" />
                        <br/>

                    </section>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-10 col-sm-offset-1" >


                    <form id="form_articulo_add" name="frmArticuloAdd" method="post" class="form-horizontal">


                        <div class="panel panel-default">
                            <div class="panel-body">

                                <center><span class="text-center display-errors show_errors"></span></center>



                                <p class="text-right">Clave: <span style="text-decoration: underline;">
                                        <?php
                                        $num =$info_articulo->data[0]->id;

                                        if($num < 10){
                                            echo '000'.$num;
                                        }else if($num < 100){
                                            echo '00'.$num;
                                        }else if($num < 1000){
                                            echo '0'.$num;
                                        }



                                        ?></span></p>
                                <br/>


                                <div class="form-group">
                                    <label  class="col-sm-3 control-label">Descripción</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="articulo_descripcion" placeholder="Ingresa descripción..." value="<?php echo $info_articulo->data[0]->descripcion ?>" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Modelo</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="articulo_modelo" placeholder="Ingresa modelo..." value="<?php echo $info_articulo->data[0]->modelo ?>" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Precio</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="articulo_precio" placeholder="Ingresa precio..." value="<?php echo $info_articulo->data[0]->precio ?>" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Existencia</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="articulo_existencia" placeholder="Ingresa existencia..." value="<?php echo $info_articulo->data[0]->existencia ?>" readonly>
                                    </div>
                                </div>

                                <input type="hidden" name="userid" value="'<?php echo $_SESSION['user_id'] ?>'"/>

                            </div>
                        </div>





                    </form>

                    <div class="form-group">
                        <div class="row">

                            <div class="col-sm-12 text-center">
                                <a href="/vendimia/articulos/"><button type="submit" name="submitButton" class="btn btn-default" id="btn-ayuda">Volver</button></a>
                            </div>


                        </div>
                    </div>

                </div>
            </div>



        </div>

    </div>
</section>


<!-- Messages -->
<section id="messages_inicio">
    <?php
    include('../../includes/messages/success.php');
    include('../../includes/messages/error.php');
    include('../../includes/messages/warning.php');
    ?>
</section>


<!-- Scripts Generales -->
<section class="scripts">
    <?php include('../../includes/partials/scripts_sub1.php') ?>
    <script src="../controllers/clientes.js"></script>

</section>

</body>
</html>