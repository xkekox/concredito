<?php
session_start();
ob_start();
//print_r($_SESSION['session']);
if($_SESSION['user_id'] != null){
    //header("location: dashboard.php");
}else{
    header("location: login");
}

include_once('../../back/Articulo.php');
$Articulo = new Articulo();
$articulos = $Articulo->index();


?>

<!doctype html>
<html lang="en">
<head>
    <?php include('../../includes/partials/styles_sub1.html') ?>
    <link rel="stylesheet" href="../assets/css/articulos.css"/>
</head>
<body id="articulos_index">

<!--  Navbar -->
<?php include('../../includes/partials/menu.php'); ?>



<!-- Contenido Principal -->
<section>
    <div class="main_wrapper">

        <div class="container-fluid" >
            <!-- Breadcrumb -->
            <div class="row">
                <ol class="breadcrumb" >
                    <li><a href="/vendimia/">Inicio</a></li>
                    <li>Articulos</li>
                    <span class="option_add pull-right" ><a href="/vendimia/articulos/create">Nuevo Articulo <i class="fa fa-plus" aria-hidden="true"></i></a></span>
                </ol>


            </div>


        </div>

        <div class="container-fluid">




            <!-- Main title -->
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <section class="main-title">

                        <h1 align="left">Articulos Registrados</h1>
                        <hr align="left" style="margin-top: 0px; margin-bottom: 10px; width: 100%; border-top:3px solid #83b35b;" />
                        <br/>

                    </section>
                </div>
            </div>


            <!-- Table information -->
            <section class="articulos_wrapper">

                <div class="clearfix"></div>

                <div class="row" >
                    <div class="col-sm-10 col-sm-offset-1" >

                        <section class="table_information_articulos" >

                            <div class="row">





                                <section class="">
                                    <table id="table_articulos_list" class="table table-bordered dt-responsive table_sistemas_list" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th class="text-center" style="vertical-align: middle;" width="20%">Clave Articulo</th>
                                            <th class="text-left" style="vertical-align: middle;" width="70%">Descripción</th>
                                            <th class="text-left" style="vertical-align: middle;" width="10%">Consultar</th>



                                        </tr>
                                        </thead>
                                        <tbody class="table_info_articulos">
                                        <?php
                                        $code_table = '';

                                        if(count(@$articulos->data) > 0){
                                            foreach($articulos->data as $data){
                                                $code_table .='
                                                    <tr>
                                                      <td align="center" class="text-center" style="vertical-align: middle; " scope="row">';
                                                $num =$data->id;

                                                if($num < 10){
                                                    $code_table .= '000'.$num;
                                                }else if($num < 100){
                                                    $code_table .= '00'.$num;
                                                }else if($num < 1000){
                                                    $code_table .= '0'.$num;
                                                }
                                                $code_table .='</td>
                                                      <td align="left" class="text-left" style="vertical-align: middle; ">'.$data->descripcion.'</td>

                                                      <td align="center" class="text-center" style="vertical-align: middle; ">
                                                        <a href="/vendimia/articulos/'. $data->id.'"><button type="button" class="btn btn_consultar" lang="'. $data->id.' "><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                                      </td>
                                                    </tr>
                                                    ';
                                            }
                                            echo $code_table;
                                        }



                                        ?>
                                        </tbody>
                                    </table>

                                </section>





                            </div>
                            <br/>
                        </section>
                    </div>
                </div>
            </section>



        </div>

    </div>
</section>


<!-- Messages -->
<section id="messages_articulos">
    <?php
    include('../../includes/messages/success.php');
    include('../../includes/messages/error.php');
    include('../../includes/messages/warning.php');
    ?>
</section>


<!-- Scripts Generales -->
<section class="scripts">
    <?php include('../../includes/partials/scripts_sub1.php') ?>
    <script src="../controllers/articulos.js"></script>

</section>

</body>
</html>