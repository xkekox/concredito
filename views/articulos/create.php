<?php
session_start();
ob_start();
//print_r($_SESSION['session']);
if($_SESSION['user_id'] != null){
    //header("location: dashboard.php");
}else{
    header("location: login");
}

include_once('../../back/Articulo.php');
$Articulo = new Articulo();
$num_articulo = $Articulo->get_num_articulo();



?>

<!doctype html>
<html lang="en">
<head>
    <?php include('../../includes/partials/styles_sub1.html') ?>
    <link rel="stylesheet" href="../assets/css/articulos.css"/>
</head>
<body id="articulos_create">

<!--  Navbar -->
<?php include('../../includes/partials/menu.php'); ?>



<!-- Contenido Princiapl -->
<section>
    <div class="main_wrapper">

        <div class="container-fluid" >
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="/vendimia/">Inicio</a></li>
                    <li><a href="/vendimia/articulos/">Articulos</a></li>
                    <li class="active">Agregar Articulo</li>
                </ol>
            </div>
        </div>


        <div class="container-fluid">




            <!-- Main title -->
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <section class="main-title">

                        <h1 align="left" style="border-left: 4px solid #83b35b; padding-left: 10px;">Registro de Articulos</h1>


                    </section>
                </div>
            </div>

            <!-- Formulario -->
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1" >


                    <form id="form_articulo_add" name="frmArticuloAdd" method="post" class="form-horizontal">


                        <div class="panel panel-default">
                            <div class="panel-body">

                                <center><span class="text-center display-errors show_errors"></span></center>



                                <p class="text-right">Clave: <span style="text-decoration: underline;">
                                        <?php
                                        if(!isset($num_articulo->data[0]->id)){
                                            $num =1;
                                        }else{
                                            $num =$num_articulo->data[0]->id+1;
                                        }

                                        if($num < 10){
                                            echo '000'.$num;
                                        }else if($num < 100){
                                            echo '00'.$num;
                                        }else if($num < 1000){
                                            echo '0'.$num;
                                        }



                                        ?></span></p>
                                <br/>


                                <div class="form-group">
                                    <label  class="col-sm-3 control-label">Descripción</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="articulo_descripcion" placeholder="Ingresa descripción...">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Modelo</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="articulo_modelo" placeholder="Ingresa modelo...">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Precio</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="articulo_precio" placeholder="Ingresa precio...">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Existencia</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="articulo_existencia" placeholder="Ingresa existencia...">
                                    </div>
                                </div>

                                <input type="hidden" name="userid" value="'<?php echo $_SESSION['user_id'] ?>'"/>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row text-center">

                                <div class="col-sm-9">

                                </div>
                                <div class="col-sm-3">
                                    <button type="button" class="btn btn-default btn-confirm" >Cancelar</button>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" name="submitButton" class="btn btn-default">Guardar</button>

                                </div>

                            </div>
                        </div>



                    </form>
                </div>
            </div>



        </div>

    </div>
</section>


<!-- Messages -->
<section id="messages_inicio">
    <?php
    include('../../includes/messages/success.php');
    include('../../includes/messages/error.php');
    include('../../includes/messages/warning.php');
    ?>
</section>


<!-- Scripts Generales -->
<section class="scripts">
    <?php include('../../includes/partials/scripts_sub1.php') ?>
    <script src="../controllers/articulos.js"></script>
    <script src="../controllers/formsValidation/articulo_form_validation.js"></script>

</section>

</body>
</html>