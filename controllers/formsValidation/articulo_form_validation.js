$(document).ready(function() {

    validar_form_articulo();
    function validar_form_articulo(){
        $('#form_articulo_add').formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: '',
                invalid: '',
                validating: 'fa fa-refresh fa-lg'
            },
            fields: {
                articulo_descripcion: {
                    validators: {
                        notEmpty: {
                            message: 'No es posible continuar, el campo <strong>descripción</strong> es obligatorio'
                        },
                        stringLength: {
                            max:255,
                            min: 2,
                            message: 'La descripción debe ser más de 2 y menos de 255 caracteres'
                        }
                    }
                },
                articulo_precio: {
                    validators: {
                        html:true,
                        notEmpty: {
                            message: 'No es posible continuar, el campo <strong>precio</strong> es obligatorio'
                        },
                        regexp: {
                            regexp: /^[\s0-9 .]+$/i,
                            message: 'El formato del campo es inválido. Solo se permiten:<br>*Digitos y punto<br> Ej. 12.50 0 12'
                        },
                        stringLength: {
                            max:20,
                            min: 1,
                            message: 'El precio debe ser igual o mayor a 1 dígito y menos de 20 digitos'
                        }
                    }
                },
                articulo_existencia: {
                    validators: {
                        html:true,
                        notEmpty: {
                            message: 'No es posible continuar, el campo <strong>existencia</strong> es obligatorio'
                        },
                        digits: {
                            message: 'El campo existencia solo debe contener dígitos'
                        },
                        stringLength: {
                            min: 1,
                            max: 10,
                            message: 'El campo de existencia debe de tener por lo menos 1 dígito'
                        }
                    }
                }



            }
        })
            .on('err.form.fv', function(e) {

                $('.show_errors').html('Favor de verificar los campos <br><br/>');

            })
            .on('success.form.fv', function(e) {
                $('.show_errors').html('');

                e.preventDefault();

                var $form = $(e.target),
                    fv	= $form.data('formValidation');



                $.ajax({
                    url: '../back/routes.php?a=post_articulo',
                    dataType: 'json',
                    type:  'post',
                    data: $form.serialize(),
                    beforeSend: function ()
                    {

                    },
                    success:  function (data)
                    {

                        if(data.success){


                            $('#message_success').html('Bien Hecho. El Articulo ha sido registrado correctamente');
                            $('.MessageSuccess').modal('show');

                            setTimeout(function(){
                                $('.MessageSuccess').modal('hide');
                                window.location.href='/vendimia/articulos/';
                            } , 3000);

                            //$('#form_cliente_add').formValidation('resetForm', true);

                        }else{

                            $('#message_error').html(data.message);
                            $('.MessageError').modal('show');

                            setTimeout(function(){
                                $('.MessageError').modal('hide');

                            } , 3000);
                        }



                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        if (XMLHttpRequest.status === 500) {
                            $('#message_warning').html(XMLHttpRequest.status);
                            $('.MessageWarning').modal('show');
                        }else{
                            $('#message_warning').html(textStatus);
                            $('.MessageWarning').modal('show');
                            console.log(XMLHttpRequest.statusText);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    }
                });
            });
    }

});



