$(document).ready(function() {

    validar_form_cliente();
    function validar_form_cliente(){
        $('#form_cliente_add').formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: '',
                invalid: '',
                validating: 'fa fa-refresh fa-lg'
            },
            fields: {
                cliente_nombre: {
                    validators: {
                        notEmpty: {
                            message: 'No es posible continuar, el campo <strong>nombre</strong> es obligatorio'
                        },
                        regexp: {
                            regexp: /^[\sa-zA-Z ñÑáéíóúÁÉÍÓÚ-]+$/i,
                            message: 'El formato del campo es inválido. Solo se permiten:<br>*letras<br>*espacio(s) en blanco'
                        },
                        stringLength: {
                            max:60,
                            min: 2,
                            message: 'El nombre debe ser más de 2 y menos de 60 caracteres'
                        }
                    }
                },
                cliente_apellido_paterno: {
                    validators: {
                        notEmpty: {
                            message: 'No es posible continuar, el campo <strong>apellido paterno</strong> es obligatorio'
                        },
                        regexp: {
                            regexp: /^[\sa-zA-Z ñÑáéíóúÁÉÍÓÚ-]+$/i,
                            message: 'El formato del campo es inválido. Solo se permiten:<br>*letras<br>*espacio(s) en blanco'
                        },
                        stringLength: {
                            max:60,
                            min: 2,
                            message: 'El apellido paterno debe ser más de 2 y menos de 60 caracteres'
                        }
                    }
                },
                cliente_apellido_materno: {
                    validators: {
                        notEmpty: {
                            message: 'No es posible continuar, el campo <strong>apellido materno</strong> es obligatorio'
                        },
                        regexp: {
                            regexp: /^[\sa-zA-Z ñÑáéíóúÁÉÍÓÚ-]+$/i,
                            message: 'El formato del campo es inválido. Solo se permiten:<br>*letras<br>*espacio(s) en blanco'
                        },
                        stringLength: {
                            max:60,
                            min: 2,
                            message: 'El apellido materno debe ser más de 2 y menos de 60 caracteres'
                        }
                    }
                },
                cliente_rfc: {
                    validators: {
                        notEmpty: {
                            message: 'No es posible continuar, el campo <strong>RFC</strong> es obligatorio'
                        },
                        regexp:{
                            regexp: (/^([a-z]{4})([0-9]{6})([a-z0-9]{3})$/i),
                            message: 'RFC inválida. El Formato del rfc es: AAAA######XXX<br> A = letra <br> # = número <br> X = letra o número'
                        },
                        stringLength: {
                            min: 13,
                            max: 13,
                            message: 'El tamaño del RFC son 13 caracteres conformado por letras y números'
                        }
                    }
                }



            }
        })
            .on('err.form.fv', function(e) {

                $('.show_errors').html('Favor de verificar los campos <br><br/>');

            })
            .on('success.form.fv', function(e) {
                $('.show_errors').html('');

                e.preventDefault();

                var $form = $(e.target),
                    fv	= $form.data('formValidation');



                $.ajax({
                    url: '../back/routes.php?a=post_cliente',
                    dataType: 'json',
                    type:  'post',
                    data: $form.serialize(),
                    beforeSend: function ()
                    {

                    },
                    success:  function (data)
                    {

                        if(data.success){


                            $('#message_success').html('Bien Hecho. El Cliente ha sido registrado correctamente');
                            $('.MessageSuccess').modal('show');

                            setTimeout(function(){
                                $('.MessageSuccess').modal('hide');
                                window.location.href='/vendimia/clientes/';
                            } , 3000);

                            //$('#form_cliente_add').formValidation('resetForm', true);

                        }else{

                            $('#message_error').html(data.message);
                            $('.MessageError').modal('show');

                            setTimeout(function(){
                                $('.MessageError').modal('hide');

                            } , 3000);
                        }



                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        if (XMLHttpRequest.status === 500) {
                            $('#message_warning').html(XMLHttpRequest.status);
                            $('.MessageWarning').modal('show');
                        }else{
                            $('#message_warning').html(textStatus);
                            $('.MessageWarning').modal('show');
                            console.log(XMLHttpRequest.statusText);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    }
                });
            });
    }

});



