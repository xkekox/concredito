$(document).ready(function() {

    validar_form_configuracion();
    function validar_form_configuracion(){
        $('#form_configuracion_add').formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: '',
                invalid: '',
                validating: 'fa fa-refresh fa-lg'
            },
            fields: {
                configuracion_tasa: {
                    validators: {
                        notEmpty: {
                            message: 'No es posible continuar, el campo <strong>Tasa de Financimiento</strong> es obligatorio'
                        },
                        regexp: {
                            regexp: /^[\s0-9 .]+$/i,
                            message: 'El formato del campo es inválido. Solo se permiten:<br>*Digitos y punto<br> Ej. 12.50 0 12'
                        },
                        stringLength: {
                            max:3,
                            min: 1,
                            message: 'La Tasa de Financimiento debe ser igual o mayor a 1 dígito y menos de 3 digitos'
                        }
                    }
                },
                configuracion_enganche: {
                    validators: {
                        html:true,
                        notEmpty: {
                            message: 'No es posible continuar, el campo <strong>% de enganche</strong> es obligatorio'
                        },
                        regexp: {
                            regexp: /^[\s0-9 .]+$/i,
                            message: 'El formato del campo es inválido. Solo se permiten:<br>*Digitos y punto<br> Ej. 12.50 0 12'
                        },
                        stringLength: {
                            max:10,
                            min: 1,
                            message: 'El precio debe ser igual o mayor a 1 dígito y menos de 10 digitos'
                        }
                    }
                },
                configuracion_plazo: {
                    validators: {
                        html:true,
                        notEmpty: {
                            message: 'No es posible continuar, el campo <strong>plazo máximo</strong> es obligatorio'
                        },
                        digits: {
                            message: 'El campo existencia solo debe contener dígitos'
                        },
                        stringLength: {
                            min: 1,
                            max: 3,
                            message: 'El campo de existencia debe de tener por lo menos 1 dígito'
                        }
                    }
                }



            }
        })
            .on('err.form.fv', function(e) {

                $('.show_errors').html('Favor de verificar los campos <br><br/>');

            })
            .on('success.form.fv', function(e) {
                $('.show_errors').html('');

                e.preventDefault();

                var $form = $(e.target),
                    fv	= $form.data('formValidation');



                $.ajax({
                    url: '../back/routes.php?a=post_configuracion',
                    dataType: 'json',
                    type:  'post',
                    data: $form.serialize(),
                    beforeSend: function ()
                    {

                    },
                    success:  function (data)
                    {


                        if(data.success){


                            $('#message_success').html(data.message);
                            $('.MessageSuccess').modal('show');

                            setTimeout(function(){
                                $('.MessageSuccess').modal('hide');
                                window.location.href='/vendimia/configuracion/create';
                            } , 3000);

                            //$('#form_cliente_add').formValidation('resetForm', true);

                        }else{

                            $('#message_error').html(data.message);
                            $('.MessageError').modal('show');

                            setTimeout(function(){
                                $('.MessageError').modal('hide');

                            } , 3000);
                        }



                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        if (XMLHttpRequest.status === 500) {
                            $('#message_warning').html(XMLHttpRequest.status);
                            $('.MessageWarning').modal('show');
                        }else{
                            $('#message_warning').html(textStatus);
                            $('.MessageWarning').modal('show');
                            console.log(XMLHttpRequest.statusText);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    }
                });
            });
    }

});



