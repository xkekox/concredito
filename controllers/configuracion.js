$(document).ready(function() {


    confirm_cancel_form();


    function confirm_cancel_form(){
        $('.btn-confirm').click(function(){

            var code_modal = '<div><i class="fa fa-exclamation-triangle" ></i> ¿Realmente desea cancelar esta operación?</div>';
            code_modal += '<div class="modal-footer">';
            code_modal += '<button type="button" class="btn btn-default btn-block" data-dismiss="modal">No</button>';
            code_modal += '<button type="button" class="btn btn-default btn-block" id="CancelConfiguracion">Si</button>';
            code_modal += '</div>';



            $('#message_warning').html(code_modal);
            $('.MessageWarning').modal('show').on('click','#CancelConfiguracion',function(){
                window.location.href = '/vendimia/configuracion/create';
            });
        });
    }


});



