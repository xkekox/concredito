$(document).ready(function(){

    view_index();
    autcomplete_clientes();
    autocomplete_articulos();

    agregar_articulo();
    abonos_mensuales();

    finish_venta();
    confirm_cancel_venta();

    function view_index(){
        $('#table_ventas_list').DataTable({
            responsive: true
        });
    }

    function autcomplete_clientes(){
        $('#autocomplete_clientes').keyup(function(e){


            var query = $(this).val();
            if(query.length > 2 ){
                if(query != '')
                {
                    e.preventDefault();
                    $.ajax({
                        url: '../back/routes.php?a=autocomplete_clientes',
                        method:'post',
                        data:{query:query},
                        success:function(data)
                        {
                            $('#clientesList').fadeIn();
                            $('#clientesList').html(data);
                        }
                    });

                    $(document).on('click', 'li.ventas_list_clientes', function(){
                        $('#autocomplete_clientes').val($(this).attr('value')+' - '+ $(this).text());
                        $('#clientesList').fadeOut();
                        $('.text_error_cliente').html('');

                        var user_rfc= $(this).attr('value');

                        if(user_rfc > 0){
                            $.ajax({
                                url: '../back/routes.php?a=get_rfc',
                                method:'post',
                                data:{userid:user_rfc},
                                beforeSend: function ()
                                {

                                },
                                success:function(data)
                                {
                                    $('#display_rfc').fadeIn();
                                    $('#display_rfc').html('<h4>RFC: '+ data+'</h4>');
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {

                                    if (XMLHttpRequest.status === 500) {

                                    }else{
                                        console.log(XMLHttpRequest.statusText);
                                        console.log(textStatus);
                                        console.log(errorThrown);
                                    }
                                }
                            });
                        }else{
                            $('#display_rfc').fadeIn();
                            $('#display_rfc').html('Cliente no valido.');
                        }



                    });
                }else{
                    $('#clientesList').html('');
                    $('#clientesList').fadeOut();
                }
            }else{
                $('#clientesList').fadeOut();
                $('#clientesList').html('');

            }

        });
    }
    function autocomplete_articulos(){
        $('#autocomplete_articulos').keyup(function(e){


            var word = $(this).val();
            if(word.length > 2 ){
                if(word != '')
                {
                    e.preventDefault();
                    $.ajax({
                        url: '../back/routes.php?a=autocomplete_articulos',
                        method:'post',
                        data:{word:word},
                        success:function(data)
                        {
                            $('#articulosList').fadeIn();
                            $('#articulosList').html(data);
                        }
                    });

                    $(document).on('click', 'li.ventas_list_articulos', function(){
                        $('#autocomplete_articulos').val($(this).attr('value')+' - '+ $(this).text());
                        $('#articulosList').fadeOut();
                        $('.text_error_articulo').html('');





                    });
                }else{
                    $('#articulosList').fadeOut();
                    $('#articulosList').html('');

                }
            }else{
                $('#articulosList').fadeOut();
                $('#articulosList').html('');

            }

        });
    }

    function agregar_articulo(){
        $('.btn_agregar_articulo').click(function(e){

            var input_cliente=$(".autocomplete_clientes").val();
            var input_articulo =$(".autocomplete_articulos").val();
            var input_boleta =$(".venta_boleta_num").val();

            var bandera1=false;
            var bandera2=false;

            if(input_cliente.length < 1){
                $('.text_error_cliente').html('Debes de ingresar un Cliente');
            }else{
                bandera1=true;

            }


            if(input_articulo.length < 1){
                $('.text_error_articulo').html('Debes de ingresar un articulo');
            }else{
                bandera2=true;
            }


            if(bandera1 && bandera2){
                $('.text_error_general').html('');


                var separador = " ";
                var limite    = 1;
                var textDeInputArticulo= input_articulo.split(separador, limite);
                var textDeInputCliente= input_cliente.split(separador, limite);

                var cliente =textDeInputCliente[0];
                var articulo_id =textDeInputArticulo[0];
                var boleta =input_boleta;
                e.preventDefault();
                 $.ajax
                 ({
                 url:   '../back/routes.php?a=agregar_articulo',
                 dataType: "json",
                 type:  'post',
                 data: {articulo:articulo_id,cliente:cliente,boleta:boleta},
                 beforeSend: function ()
                 {


                 },
                 success:  function (data)
                 {

                     var code_row='';
                     if(data.success)
                     {

                         $.each(data.data,function(index,value){
                             code_row ='<tr class="grid_'+value.id+'">';
                             code_row +='<td align="center" class="text-left" style="vertical-align: middle; ">'+value.descripcion+'</td>';
                             if(value.modelo == '' ){
                                 code_row +='<td align="center" class="text-left" style="vertical-align: middle; ">---</td>';
                             }else{
                                 code_row +='<td align="center" class="text-left" style="vertical-align: middle; ">'+value.modelo+'</td>';
                             }
                             code_row +='<td align="center" class="text-center" style="vertical-align: middle; "><input type="text" boleta="'+value.venta_id+'" id="cantidad_'+value.id+'" value="'+value.cantidad+'" lang="'+value.id+'"class="cant_art" style="width: 100px;" autofocus/></td>';
                             code_row +='<td align="center" class="text-center" style="vertical-align: middle; ">'+value.precio+'</td>';
                             code_row +='<td align="center" class="text-center" style="vertical-align: middle; "><span id="importe_'+value.id+'">'+value.importe+'</span></td>';
                             code_row +='<td align="center" class="text-center" style="vertical-align: middle; "><input type="hidden" value="'+value.id+'"/><span id="'+value.venta_id+'" lang="'+value.id+'" class="del_art" style="cursor: pointer;" ><i class="fa fa-times text-danger" aria-hidden="true"></i></span></td>';
                             code_row +='</tr>';


                         });


                         $('#table_ventas_articulos').append(code_row);
                         $('.autocomplete_articulos').val('');
                         $('.wrapper_table_info').fadeIn('slow');


                         //$('#importe_'+data.id).html(data.importe);
                         $('#venta_enganche').html(data.enganche);
                         $('#venta_bonificacion').html(data.bonificacion);
                         $('#venta_adeudo').html(data.adeudo);
                         $('.btn_step_2').fadeIn();


                     }
                     else if(data.success == false)
                     {



                         $('#message_error').html(data.message);
                         $('.MessageError').modal('show');

                         setTimeout(function(){
                             $('.MessageError').modal('hide');

                         } , 3000);
                     }
                     else
                     {
                         $('#message_error').html(data.message);
                         $('.MessageError').modal('show');

                         setTimeout(function(){
                             $('.MessageError').modal('hide');

                         } , 3000);

                     }


                 },
                 error: function (XMLHttpRequest, textStatus, errorThrown) {

                     if (XMLHttpRequest.status === 500) {

                     }else{
                         console.log(XMLHttpRequest.statusText);
                         console.log(textStatus);
                         console.log(errorThrown);
                     }
                  }


            });

                $(document).off('change', 'input.cant_art').on('change', 'input.cant_art', function(e){
                    var articulo= $(this).attr('lang');
                    var cantidad= $(this).val();
                    var boleta= $(this).attr('boleta');


                        $.ajax
                        ({
                            url:   '../back/routes.php?a=grid_cantidad_articulo',
                            dataType: "json",
                            type:  'post',
                            data: {articulo:articulo,cantidad:cantidad,boleta:boleta},
                            beforeSend: function ()
                            {


                            },
                            success:  function (data)
                            {


                                if(data.success)
                                {


                                    $('#cantidad_'+data.id).val(data.cantidad);
                                    $('#importe_'+data.id).html(data.importe);



                                    $('#venta_enganche').html(data.enganche);
                                    $('#venta_bonificacion').html(data.bonificacion);
                                    $('#venta_adeudo').html(data.adeudo);
                                    $('.btn_step_2').fadeIn();


                                    /*
                                     $('#message_success').html(data.message);
                                     $('.MessageSuccess').modal('show');

                                     setTimeout(function(){
                                     $('.MessageSuccess').modal('hide');
                                     } , 3000);
                                     */

                                }
                                else if(data.success == false)
                                {
                                    $('#cantidad_'+data.id).val(data.cantidad);
                                    $('#importe_'+data.id).html(data.importe);



                                    $('#venta_enganche').html(data.enganche);
                                    $('#venta_bonificacion').html(data.bonificacion);
                                    $('#venta_adeudo').html(data.adeudo);
                                    $('.btn_step_2').fadeIn();

                                    $('#message_error').html(data.message);
                                    $('.MessageError').modal('show');

                                    setTimeout(function(){
                                        $('.MessageError').modal('hide');

                                    } , 3000);
                                }
                                else
                                {
                                    $('#message_error').html(data.message);
                                    $('.MessageError').modal('show');

                                    setTimeout(function(){
                                        $('.MessageError').modal('hide');

                                    } , 3000);

                                }


                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {

                                if (XMLHttpRequest.status === 500) {

                                }else{
                                    console.log(XMLHttpRequest.statusText);
                                    console.log(textStatus);
                                    console.log(errorThrown);
                                }
                            }


                        });

                    /*
                    alert('el articulo: '+articulo+' se multiplica por:'+cantidad);
                    $('#importe_'+articulo).html('3000');
                    */




                });

                $(document).off('click', 'span.del_art').on('click', 'span.del_art', function(e){
                    var boleta= $(this).attr('id');
                    var articulo= $(this).attr('lang');

                    e.preventDefault();
                    $.ajax({
                        url: '../back/routes.php?a=delete_articulo',
                        method:'post',
                        dataType: "json",
                        data:{boleta:boleta,articulo:articulo},
                        beforeSend: function ()
                        {

                        },
                        success:function(data)
                        {

                            if(data.success)
                            {
                                $('.grid_'+data.id).fadeOut('slow');
                                $('#venta_enganche').html(data.enganche);
                                $('#venta_bonificacion').html(data.bonificacion);
                                $('#venta_adeudo').html(data.adeudo);

                            }
                            else if(data.success == false)
                            {

                                $('.grid_'+data.id).fadeOut('slow');
                                $('#venta_enganche').html(data.enganche);
                                $('#venta_bonificacion').html(data.bonificacion);
                                $('#venta_adeudo').html(data.adeudo);

                                $('#message_error').html(data.message);
                                $('.MessageError').modal('show');

                                setTimeout(function(){
                                    $('.MessageError').modal('hide');

                                } , 3000);
                            }
                            else
                            {
                                $('#message_error').html(data.message);
                                $('.MessageError').modal('show');

                                setTimeout(function(){
                                    $('.MessageError').modal('hide');

                                } , 3000);

                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {

                            if (XMLHttpRequest.status === 500) {

                            }else{
                                console.log(XMLHttpRequest.statusText);
                                console.log(textStatus);
                                console.log(errorThrown);
                            }
                        }
                    });




                });



            }else{
                $('.text_error_general').html('Favor de verificar los campos.');
            }


            //$('#autocomplete_articulos').val($('.li.ventas_list_articulos').attr('value'));


        });
    }

    function abonos_mensuales(){
        $('.btn_step_2').click(function(e){
            var input_cliente=$(".autocomplete_clientes").val();
            var input_boleta =$(".venta_boleta_num").val();
            var separador = " ";
            var limite    = 1;
            var textDeInputCliente= input_cliente.split(separador, limite);

            var cliente =textDeInputCliente[0];
            var boleta =input_boleta;
            e.preventDefault();


            if(input_cliente.length < 1){
                $('.text_error_cliente').html('Debes de ingresar un Cliente');
                $('#message_error').html('Debes de ingresar un Cliente');
                $('.MessageError').modal('show');

                setTimeout(function(){
                    $('.MessageError').modal('hide');

                } , 3000);
            }else{
                $.ajax({
                    url: '../back/routes.php?a=get_abonos_mensuales',
                    method:'post',
                    dataType: "json",
                    data:{boleta:boleta,cliente:cliente},
                    beforeSend: function ()
                    {

                    },
                    success:function(data)
                    {


                        if(data.success)
                        {
                            $('.autocomplete_clientes_section').fadeOut();

                            $('.importe_abono_3').html(data.importe_abono_3);
                            $('.importe_abono_6').html(data.importe_abono_6);
                            $('.importe_abono_9').html(data.importe_abono_9);
                            $('.importe_abono_12').html(data.importe_abono_12);


                            $('.total_pago_3').html('TOTAL A PAGAR '+ data.total_pagar_3);
                            $('.total_pago_6').html('TOTAL A PAGAR '+ data.total_pagar_6);
                            $('.total_pago_9').html('TOTAL A PAGAR '+ data.total_pagar_9);
                            $('.total_pago_12').html('TOTAL A PAGAR '+ data.total_pagar_12);


                            $('.ahorro_3').html('SE AHORRA $'+data.importe_ahorro_3);
                            $('.ahorro_6').html('SE AHORRA $'+data.importe_ahorro_6);
                            $('.ahorro_9').html('SE AHORRA $'+data.importe_ahorro_9);
                            $('.ahorro_12').html('SE AHORRA $'+data.importe_ahorro_12);

                            $('.abonos_mensuales').fadeIn();
                            $('.btn_step_2').fadeOut();
                            $('.last_step').fadeIn();
                            /*
                            $('#message_success').html(data.message);
                            $('.MessageSuccess').modal('show');

                            setTimeout(function(){
                                $('.MessageSuccess').modal('hide');
                            } , 3000);
                            */
                        }
                        else if(data.success == false)
                        {
                            $('#message_error').html(data.message);
                            $('.MessageError').modal('show');

                            setTimeout(function(){
                                $('.MessageError').modal('hide');

                            } , 3000);

                        }
                        else
                        {
                            $('#message_error').html(data.message);
                            $('.MessageError').modal('show');

                            setTimeout(function(){
                                $('.MessageError').modal('hide');

                            } , 3000);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                        if (XMLHttpRequest.status === 500) {

                        }else{
                            console.log(XMLHttpRequest.statusText);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    }
                });

            }

        });
    }

    function finish_venta(){
        $('.btn_last_step').click(function(){
            if($("input[name='abono_mensual']:radio").is(':checked')) {

                var input_cliente=$(".autocomplete_clientes").val();
                var separador = " ";
                var limite    = 1;
                var textDeInputCliente= input_cliente.split(separador, limite);
                var cliente =textDeInputCliente[0];


                var boleta =$(".venta_boleta_num").val();
                var plazos =$('input:radio[name=abono_mensual]:checked').val();

                $.ajax({
                    url: '../back/routes.php?a=post_venta',
                    method:'post',
                    dataType: "json",
                    data:{boleta:boleta,plazos:plazos,cliente:cliente},
                    beforeSend: function ()
                    {

                    },
                    success:function(data)
                    {


                        if(data.success)
                        {

                            $('#message_success').html('Bien Hecho. Tu venta ha sido registrada correctamente');
                            $('.MessageSuccess').modal('show');

                            setTimeout(function(){
                                $('.MessageSuccess').modal('hide');
                                window.location.href='/vendimia/ventas/';
                            } , 3000);
                        }
                        else if(data.success == false)
                        {
                            $('#message_error').html(data.message);
                            $('.MessageError').modal('show');

                            setTimeout(function(){
                                $('.MessageError').modal('hide');

                            } , 3000);

                        }
                        else
                        {
                            $('#message_error').html(data.message);
                            $('.MessageError').modal('show');

                            setTimeout(function(){
                                $('.MessageError').modal('hide');

                            } , 3000);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                        if (XMLHttpRequest.status === 500) {

                        }else{
                            console.log(XMLHttpRequest.statusText);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    }
                });
            } else {
                $('#message_error').html('Debe Seleccionar un plazo para realizar el pago de su compra');
                $('.MessageError').modal('show');

                setTimeout(function(){
                    $('.MessageError').modal('hide');

                } , 3000);
            }
        });
    }

    function confirm_cancel_venta(){
        $('.btn-confirm').click(function(){

            var code_modal = '<div><i class="fa fa-exclamation-triangle" ></i> ¿Realmente desea cancelar esta operación?</div>';
            code_modal += '<div class="modal-footer">';
            code_modal += '<button type="button" class="btn btn-default btn-block" data-dismiss="modal">No</button>';
            code_modal += '<button type="button" class="btn btn-default btn-block" id="CancelVenta">Si</button>';
            code_modal += '</div>';



            $('#message_warning').html(code_modal);
            $('.MessageWarning').modal('show').on('click','#CancelVenta',function(){

                var boleta =$(".venta_boleta_num").val();

                $.ajax({
                    url: '../back/routes.php?a=cancel_venta',
                    method:'post',
                    dataType: "json",
                    data:{boleta:boleta},
                    beforeSend: function ()
                    {

                    },
                    success:function(data)
                    {


                        if(data.success)
                        {


                            $.each(data.data,function(index,value){


                                $.ajax({
                                    url: '../back/routes.php?a=delete_articulo',
                                    method:'post',
                                    dataType: "text",
                                    data:{boleta:value.venta_id,articulo:value.articulo_id},
                                    beforeSend: function ()
                                    {

                                    },
                                    success:function(data)
                                    {

                                        if(data.success)
                                        {


                                        }
                                        else if(data.success == false)
                                        {


                                        }
                                        else
                                        {


                                        }
                                    },
                                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                                        if (XMLHttpRequest.status === 500) {

                                        }else{
                                            console.log(XMLHttpRequest.statusText);
                                            console.log(textStatus);
                                            console.log(errorThrown);
                                        }
                                    }
                                });

                            });



                        }
                        else if(data.success == false)
                        {

                            $('#message_error').html(data.message);
                            $('.MessageError').modal('show');

                            setTimeout(function(){
                                $('.MessageError').modal('hide');

                            } , 3000);


                        }
                        else
                        {

                            $('#message_error').html(data.message);
                            $('.MessageError').modal('show');

                            setTimeout(function(){
                                $('.MessageError').modal('hide');

                            } , 3000);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                        if (XMLHttpRequest.status === 500) {

                        }else{
                            console.log(XMLHttpRequest.statusText);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    },
                    complete: function(){

                            window.location.href='/vendimia/ventas/';

                    }
                });
            });
        });
    }
});
