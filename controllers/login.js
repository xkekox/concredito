$(document).ready(function() {

    login();


    function login(){
        $('.btn-login').click(function(){
            var username = $('[name="username"]').val();
            var password = $('[name="password"]').val();

            var bandera1 = false;
            var bandera2 = false;

            if(username.length < 1){
                $('.text_error_username').text('*El username es obligatorio')
            }else if(username.length < 5){
                $('.text_error_username').text('*El nombre de usuario es demasiado corto')
            }else{
                $('.text_error_username').text('');
                bandera1=true;
            }

            if(password.length < 1){
                $('.text_error_password').text('*El password es obligatorio')
            }else{
                $('.text_error_password').text('');
                bandera2=true;
            }

            if(bandera1 && bandera2){
                $.ajax
                ({
                    url:   'back/routes.php?a=login',
                    dataType: "json",
                    type:  'post',
                    data: {'username':username,'password':password},
                    beforeSend: function ()
                    {


                    },
                    success: function(data)
                    {


                        if(data.success == true){



                            if(data.status == 0){
                                $('.message_login').html('Usuario dado de Baja.');
                            }else{
                                $('.message_login').html(data.message);
                                window.location.href='../vendimia/';
                            }



                            /*
                             setTimeout(function(){
                             location.reload();
                             }, 3000);
                             */




                        }else if(data.success == false){

                            $('.message_login').html(data.message);

                        }else{

                            $('.message_login').html(data.message);
                        }

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        if (XMLHttpRequest.status === 500) {
                            alert('error 500');
                        }else{
                            //$('#message_warning').html(textStatus);
                            //$('.MessageWarning').modal('show');
                            console.log(XMLHttpRequest.statusText);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    }


                });

            }

        });
    }


});