$(document).ready(function() {

    view_index();
    confirm_cancel_form();

    function view_index(){
        //$('#table_clientes_list').DataTable().clear().destroy();
        $('#table_clientes_list').DataTable({
            responsive: true
        });
    }

    function confirm_cancel_form(){
        $('.btn-confirm').click(function(){

            var code_modal = '<div><i class="fa fa-exclamation-triangle" ></i> ¿Realmente desea cancelar esta operación?</div>';
            code_modal += '<div class="modal-footer">';
            code_modal += '<button type="button" class="btn btn-default btn-block" data-dismiss="modal">No</button>';
            code_modal += '<button type="button" class="btn btn-default btn-block" id="CancelCliente">Si</button>';
            code_modal += '</div>';



            $('#message_warning').html(code_modal);
            $('.MessageWarning').modal('show').on('click','#CancelCliente',function(){
                window.location.href = '/vendimia/clientes/';
            });
        });
    }


});



