-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-03-2017 a las 12:55:50
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `vendimia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_articulos`
--

CREATE TABLE IF NOT EXISTS `tb_articulos` (
  `id` int(10) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `modelo` varchar(50) DEFAULT NULL,
  `precio` float NOT NULL,
  `existencia` int(5) NOT NULL,
  `user_id` int(10) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tb_articulos`
--

INSERT INTO `tb_articulos` (`id`, `descripcion`, `modelo`, `precio`, `existencia`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 'comedor 4 sillas', '', 4250, 4, 1, '2017-03-24 15:07:26', NULL),
(3, 'Microondas', 'mabe', 1500, 5, 1, '2017-03-24 21:52:57', NULL),
(4, 'Refrigerador', 'whirpool', 2700.5, 0, 1, '2017-03-24 21:53:22', NULL),
(5, 'comedor 2 sillas', '', 2000, 5, 1, '2017-03-25 09:48:28', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_clientes`
--

CREATE TABLE IF NOT EXISTS `tb_clientes` (
  `id` int(10) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido_paterno` varchar(50) NOT NULL,
  `apellido_materno` varchar(50) NOT NULL,
  `rfc` varchar(50) NOT NULL,
  `usuario_id` int(10) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tb_clientes`
--

INSERT INTO `tb_clientes` (`id`, `nombre`, `apellido_paterno`, `apellido_materno`, `rfc`, `usuario_id`, `created_at`, `updated_at`) VALUES
(2, 'Daniel Alejandro', 'Paredes', 'Rivas', 'PARD920116DU4', 1, '2017-03-23 22:16:17', NULL),
(3, 'Luis', 'Paredes', 'Rivas', 'rfcluis', 1, '2017-03-23 22:37:48', NULL),
(4, 'Ricardo', 'Paredes', 'Rivas', 'rfcrichard', 1, '2017-03-24 15:37:03', NULL),
(5, 'Daniel', 'Lopez', 'Salazar', 'DALOSA1234', 1, '2017-03-25 09:50:07', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_configuraciones`
--

CREATE TABLE IF NOT EXISTS `tb_configuraciones` (
  `id` int(10) NOT NULL,
  `tasa_financiamiento` float NOT NULL,
  `porcentaje_enganche` float NOT NULL,
  `plazo_maximo` int(5) NOT NULL,
  `user_id` int(10) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tb_configuraciones`
--

INSERT INTO `tb_configuraciones` (`id`, `tasa_financiamiento`, `porcentaje_enganche`, `plazo_maximo`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 2.8, 20, 12, 1, '2017-03-24 18:12:52', '2017-03-25 14:50:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_usuarios`
--

CREATE TABLE IF NOT EXISTS `tb_usuarios` (
  `id` int(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido_paterno` varchar(100) NOT NULL,
  `apellido_materno` varchar(100) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tb_usuarios`
--

INSERT INTO `tb_usuarios` (`id`, `username`, `password`, `nombre`, `apellido_paterno`, `apellido_materno`, `status`, `created_at`, `updated_at`) VALUES
(1, 'danielparedes', 'a7cb6fd76ad58456b17d2cbd35c3658c', 'Daniel', 'Paredes', 'Rivas', 1, '2017-03-23 17:17:14', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_ventas`
--

CREATE TABLE IF NOT EXISTS `tb_ventas` (
  `id` int(10) NOT NULL,
  `cliente_id` int(10) NOT NULL,
  `plazos` int(3) NOT NULL,
  `abonos` int(10) NOT NULL,
  `total` double(10,2) NOT NULL,
  `ahorro` double(10,2) NOT NULL,
  `fecha` date NOT NULL,
  `estatus` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tb_ventas`
--

INSERT INTO `tb_ventas` (`id`, `cliente_id`, `plazos`, `abonos`, `total`, `ahorro`, `fecha`, `estatus`) VALUES
(1, 2, 12, 347, 4160.84, 0.00, '2017-03-27', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_ventas_detalle`
--

CREATE TABLE IF NOT EXISTS `tb_ventas_detalle` (
  `id` int(10) NOT NULL,
  `venta_id` int(10) NOT NULL,
  `articulo_id` int(10) NOT NULL,
  `precio` int(10) NOT NULL,
  `cantidad` int(10) NOT NULL,
  `importe` int(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tb_ventas_detalle`
--

INSERT INTO `tb_ventas_detalle` (`id`, `venta_id`, `articulo_id`, `precio`, `cantidad`, `importe`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 5678, 1, 5678, '2017-03-27 11:13:23', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_ventas_deuda`
--

CREATE TABLE IF NOT EXISTS `tb_ventas_deuda` (
  `id` int(10) NOT NULL,
  `venta_id` int(10) NOT NULL,
  `enganche` double(10,2) NOT NULL,
  `bonificacion` double(10,2) NOT NULL,
  `total` double(10,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tb_ventas_deuda`
--

INSERT INTO `tb_ventas_deuda` (`id`, `venta_id`, `enganche`, `bonificacion`, `total`, `created_at`, `updated_at`) VALUES
(1, 1, 1135.60, 381.56, 4160.84, '2017-03-27 11:13:25', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tb_articulos`
--
ALTER TABLE `tb_articulos`
  ADD PRIMARY KEY (`id`), ADD KEY `fk_articulos` (`user_id`);

--
-- Indices de la tabla `tb_clientes`
--
ALTER TABLE `tb_clientes`
  ADD PRIMARY KEY (`id`), ADD KEY `fk1` (`usuario_id`);

--
-- Indices de la tabla `tb_configuraciones`
--
ALTER TABLE `tb_configuraciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tb_usuarios`
--
ALTER TABLE `tb_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tb_ventas`
--
ALTER TABLE `tb_ventas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tb_ventas_detalle`
--
ALTER TABLE `tb_ventas_detalle`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tb_ventas_deuda`
--
ALTER TABLE `tb_ventas_deuda`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tb_articulos`
--
ALTER TABLE `tb_articulos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `tb_clientes`
--
ALTER TABLE `tb_clientes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `tb_configuraciones`
--
ALTER TABLE `tb_configuraciones`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tb_usuarios`
--
ALTER TABLE `tb_usuarios`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tb_ventas_detalle`
--
ALTER TABLE `tb_ventas_detalle`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tb_ventas_deuda`
--
ALTER TABLE `tb_ventas_deuda`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tb_articulos`
--
ALTER TABLE `tb_articulos`
ADD CONSTRAINT `fk_articulos` FOREIGN KEY (`user_id`) REFERENCES `tb_usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tb_clientes`
--
ALTER TABLE `tb_clientes`
ADD CONSTRAINT `fk1` FOREIGN KEY (`usuario_id`) REFERENCES `tb_usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
