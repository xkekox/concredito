<?php
define("DB_SERVER","localhost");
define("DB_USER","root");
define("DB_PASS","");
define("DB_NAME","vendimia");

class Conexion{


    function conexion() {
        $conexion = new mysqli(DB_SERVER,DB_USER,DB_PASS,DB_NAME);

        if ($conexion->connect_errno) {
            printf("Problemas con la conexión a la base de datos: %s\n", $conexion->connect_error);
            exit();
        }


        return $conexion;
    }


}

?>