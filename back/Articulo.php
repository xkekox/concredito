<?php
require_once('connection.php');
ini_set('display_errors', 1);
error_reporting(E_ALL);

class Articulo extends Conexion{

    public function index(){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $query ="select id,descripcion
                from tb_articulos";


        $registros = $conexion->query($query);

        $resultado=array();
        if($registros->num_rows > 0) {
            $resultado['success'] = true;
            while($row = $registros->fetch_assoc()){
                $resultado['data'][] = $row;
            }
            $resultado['message'] = "Se encontraron articulos.";

        }else{
            $resultado['success'] = false;
            $resultado['message'] = "No existen articulos";
        }




        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;
    }

    public function store($descripcion,$modelo,$precio,$existencia,$usuario){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');


        $query="INSERT INTO
                    tb_articulos(descripcion,modelo,precio,existencia,user_id,created_at)
                VALUES
                    ('$descripcion','$modelo','$precio','$existencia',$usuario,now())";

        $resultado=array();
        if(!$conexion->query($query)){
            $resultado['success'] = false;
            $resultado['message'] = $conexion->error;
            $resultado['error'] = $conexion->error;

        }else{
            $resultado['success'] = true;
            $resultado['message'] = 'Bien Hecho. El Articulo ha sido registrado correctamente';

        }



        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;
    }

    public function show($articulo_id){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $query ="select id,descripcion,modelo,precio,existencia
                from tb_articulos
                where id ='$articulo_id'";


        $registros = $conexion->query($query);

        $resultado=array();
        if($registros->num_rows > 0) {
            $resultado['success'] = true;
            if($row = $registros->fetch_assoc()){
                $resultado['data'][] = $row;
            }
            $resultado['message'] = "Se encontraro el articulo.";

        }else{
            $resultado['success'] = false;
            $resultado['message'] = "No existe el articulo";
        }




        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;
    }

    public function get_num_articulo(){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $query ="select id
                from tb_articulos
                order by id desc
                limit 1";


        $registros = $conexion->query($query);

        $resultado=array();
        if($registros->num_rows > 0) {
            $resultado['success'] = true;
            if($row = $registros->fetch_assoc()){
                $resultado['data'][] = $row;
            }
            $resultado['message'] = "Se encontraro el articulo.";

        }else{
            $resultado['success'] = false;
            $resultado['message'] = "No existe el articulo";
        }




        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;
    }


}

?>