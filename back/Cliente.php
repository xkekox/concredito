<?php
require_once('connection.php');
ini_set('display_errors', 1);
error_reporting(E_ALL);

class Cliente extends Conexion{

    public function index(){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $query ="select id,nombre,apellido_paterno,apellido_materno
                from tb_clientes";


        $registros = $conexion->query($query);

        $resultado=array();
        if($registros->num_rows > 0) {
            $resultado['success'] = true;
            while($row = $registros->fetch_assoc()){
                $resultado['data'][] = $row;
            }
            $resultado['message'] = "Se encontraron registros.";

        }else{
            $resultado['success'] = false;
            $resultado['message'] = "No existen clientes";
        }




        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;
    }

    public function store($nombre,$apellido_paterno,$apellido_materno,$rfc,$usuario){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');


        $query="INSERT INTO
                    tb_clientes(nombre,apellido_paterno,apellido_materno,rfc,usuario_id,created_at)
                VALUES
                    ('$nombre','$apellido_paterno','$apellido_materno','$rfc',$usuario,now())";

        $resultado=array();
        if(!$conexion->query($query)){
            $resultado['success'] = false;
            $resultado['message'] = $conexion->error;
            $resultado['error'] = $conexion->error;

        }else{
            $resultado['success'] = true;
            $resultado['message'] = 'Bien Hecho. El Cliente ha sido registrado correctamente';

        }



        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;
    }

    public function show($cliente_id){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $query ="select id,nombre,apellido_paterno,apellido_materno,rfc
                from tb_clientes
                where id='$cliente_id'";


        $registros = $conexion->query($query);

        $resultado=array();
        if($registros->num_rows > 0) {
            $resultado['success'] = true;
            if($row = $registros->fetch_assoc()){
                $resultado['data'][] = $row;
            }
            $resultado['message'] = "Se encontraro el cliente.";

        }else{
            $resultado['success'] = false;
            $resultado['message'] = "No existe el cliente";
        }




        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;
    }

    public function get_num_boleta(){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $query ="select id
                from tb_clientes
                order by id desc
                limit 1";


        $registros = $conexion->query($query);

        $resultado=array();
        if($registros->num_rows > 0) {
            $resultado['success'] = true;
            if($row = $registros->fetch_assoc()){
                $resultado['data'][] = $row;
            }
            $resultado['message'] = "Se encontraron registros.";

        }else{
            $resultado['success'] = false;
            $resultado['message'] = "No existen clientes";
        }




        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;
    }


}

?>