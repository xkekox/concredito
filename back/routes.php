<?php
require_once("Cliente.php");
require_once("Articulo.php");
require_once("Configuracion.php");
require_once("Venta.php");
require_once("login.php");

$cliente = new Cliente();
$articulo = new Articulo();
$configuracion = new Configuracion();
$venta = new Ventas();
$login =new Login();

$action = isset($_GET['a']) ? $_GET['a'] : null;


switch($action) {
	case 'login':
        $username = htmlspecialchars(trim($_POST['username']));
        $password  = htmlspecialchars(trim($_POST['password']));

        if(isset($username) && isset($password)){
            $resultado = $login->validarLogin($username,$password);
        }else{
            $resultado['success'] = false;
            $resultado['message'] = 'Favor de volver intentarlo';
        }

        echo json_encode($resultado);
    break;
    
    case 'post_cliente':

        //data

        $nombre = $_POST['cliente_nombre'];
        $apellido_paterno = $_POST['cliente_apellido_paterno'];
        $apellido_materno = $_POST['cliente_apellido_materno'];
        $rfc= $_POST['cliente_rfc'];
        $userid = $_POST['userid'];


        if(isset($userid) && isset($nombre) && isset($apellido_paterno) && isset($apellido_materno) && isset($rfc)){
            $resultado = $cliente->store($nombre,$apellido_paterno,$apellido_materno,$rfc,$userid);
        }else{
            $resultado['success'] = false;
            $resultado['message'] = 'Error vuelva a intentar';
        }



        echo json_encode($resultado);
    break;

    case 'post_articulo':

        //data

        $descripcion = $_POST['articulo_descripcion'];
        $modelo = $_POST['articulo_modelo'];
        $precio = $_POST['articulo_precio'];
        $existencia = $_POST['articulo_existencia'];
        $userid = $_POST['userid'];


        if(isset($descripcion) && isset($precio) && isset($existencia) && isset($userid) ){
            $resultado = $articulo->store($descripcion,$modelo,$precio,$existencia,$userid);
        }else{
            $resultado['success'] = false;
            $resultado['message'] = 'Error vuelva a intentar';
        }



        echo json_encode($resultado);
        break;

    case 'post_configuracion':

        //data

        $tasa = $_POST['configuracion_tasa'];
        $enganche = $_POST['configuracion_enganche'];
        $plazo = $_POST['configuracion_plazo'];
        $userid = $_POST['userid'];


        if(isset($tasa) && isset($enganche) && isset($plazo) && isset($userid) ){
            $resultado = $configuracion->store($tasa,$enganche,$plazo,$userid);
        }else{
            $resultado['success'] = false;
            $resultado['message'] = 'Error vuelva a intentar';
        }



        echo json_encode($resultado);
        break;

    case 'autocomplete_clientes':

        $word = $_POST['query'];

        if(isset($word)){
            $resultado = $venta->autocomplete_clientes($word);
        }else{
            $resultado['success'] = false;
            $resultado['message'] = 'Error vuelva a intentar';
        }

        echo $resultado;
    break;

    case 'get_rfc':

        $userid = $_POST['userid'];

        if(isset($userid)){
            $resultado = $venta->get_rfc($userid);
        }else{
            $resultado = '';

        }

        echo $resultado;
    break;

    case 'autocomplete_articulos':

        $word = $_POST['word'];

        if(isset($word)){
            $resultado = $venta->autocomplete_articulos($word);
        }else{
            $resultado['success'] = false;
            $resultado['message'] = 'Error vuelva a intentar';
        }

        echo $resultado;
        break;



    case 'agregar_articulo':

        $cliente = $_POST['cliente'];
        $articulo = $_POST['articulo'];
        $folio = $_POST['boleta'];


        if(isset($articulo)){
            $check_cliente= $venta->validar_cliente($cliente);
            if($check_cliente->status == 0){
                $resultado['success'] = false;
                $resultado['message'] = 'Por favor de ingresar un cliente valido';
            }else{
                $check_articulo = $venta->validar_articulo($articulo);
                if($check_articulo->status == 0){
                    $resultado['success'] = false;
                    $resultado['message'] = 'Por favor de ingresar un articulo valido';
                }else{
                    $existencia = $venta->validar_existencia($articulo);
                    $cond_existencia = $existencia->status;
                    if($cond_existencia > 0){
                        $resultado = $venta->grid_venta($folio,$articulo,1);
                    }else{
                        $resultado['message'] = 'El articulo seleccionado no cuenta con existencia, favor de verificar';
                    }
                }

            }


        }else{
            $resultado['success'] = false;
            $resultado['message'] = 'Error vuelva a intentar';
        }

        echo json_encode($resultado);
    break;

    case 'delete_articulo':
        $boleta = $_POST['boleta'];
        $articulo = $_POST['articulo'];

        if(isset($articulo)){

            $resultado = $venta->delete_articulo_grid($boleta,$articulo);

        }else{
            $resultado['success'] = false;
            $resultado['message'] = 'Error vuelva a intentar';
        }

        echo json_encode($resultado);
    break;


    case 'grid_cantidad_articulo':
        $articulo = $_POST['articulo'];
        $cantidad = $_POST['cantidad'];
        $boleta = $_POST['boleta'];


        if(isset($articulo)){

                $resultado = $venta->grid_cantidad_articulo($boleta,$articulo,$cantidad);

        }else{
            $resultado['success'] = false;
            $resultado['message'] = 'Error vuelva a intentar';
        }

        echo json_encode($resultado);

    break;

    case'get_abonos_mensuales':
        $cliente = $_POST['cliente'];
        $boleta = $_POST['boleta'];

        if(isset($boleta)){
            $check_cliente= $venta->validar_cliente($cliente);
            if($check_cliente->status == 0){
                $resultado['success'] = false;
                $resultado['message'] = 'Los datos ingresados no son correctos,favor de verificar el cliente';
            }else{
                $check_grid = $venta->validar_grid_con_articulos($boleta);
                if($check_grid->status == 0){
                    $resultado['success'] = false;
                    $resultado['message'] = 'Los datos ingresados no son correctos,favor de ingresar por lo menos 1 articulo';
                }else{
                    $resultado = $venta->get_abonos($boleta);
                }

            }
        }else{
            $resultado['success'] = false;
            $resultado['message'] = 'Error vuelva a intentar';
        }





        echo json_encode($resultado);
    break;

    case 'post_venta':
        $plazos= $_POST['plazos'];
        $boleta = $_POST['boleta'];
        $cliente = $_POST['cliente'];

        if(isset($boleta)){
            $resultado = $venta->store_venta($boleta,$plazos,$cliente);
        }else{
            $resultado['success'] = false;
            $resultado['message'] = 'Error vuelva a intentar';
        }

        echo json_encode($resultado);
    break;

    case 'cancel_venta':
        $boleta = $_POST['boleta'];

        if(isset($boleta)){
            $resultado = $venta->cancel_venta($boleta);
        }else{
            $resultado['success'] = false;
            $resultado['message'] = 'Error vuelva a intentar';
        }

        echo json_encode($resultado);
    break;

}

?>