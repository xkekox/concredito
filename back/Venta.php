<?php
require_once('connection.php');
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_DEPRECATED);

class Ventas extends Conexion{

    /*** GENERAL ***/
    public function index(){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $query ="select tbv.id as folio,tbc.id as cliente,CONCAT(tbc.nombre, ' ', tbc.apellido_paterno, ' ', tbc.apellido_materno) as nombre,tbv.total,tbv.fecha
                from tb_ventas tbv
                inner join tb_clientes tbc on tbc.id = tbv.cliente_id";


        $registros = $conexion->query($query);

        $resultado=array();
        if($registros->num_rows > 0) {
            $resultado['success'] = true;
            while($row = $registros->fetch_assoc()){
                $resultado['data'][] = $row;
            }
            $resultado['message'] = "Se encontraron registros.";

        }else{
            $resultado['success'] = false;
            $resultado['message'] = "No existen clientes";
        }




        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;
    }
    public function boleta_venta_actual(){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');


        $query = "select id
                    from tb_ventas
                    order by id DESC
                    limit 1
                    ";

        $registros = $conexion->query($query);

        $resultado='';
        if($registros->num_rows > 0)
        {
            if($row = $registros->fetch_assoc())
            {
                $resultado = $row['id'];
            }
        }
        else
        {
            $resultado = 0;
        }

        return $resultado;
    }

    /*** AUTOCOMPLETES (GET´S) ***/
    public function autocomplete_clientes($word){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');




        $query = "SELECT id,CONCAT(nombre, ' ', apellido_paterno, ' ', apellido_materno) as nombre
                    FROM tb_clientes
                    where (nombre like '%".$word."%') or (apellido_paterno like '%".$word."%')
                    ";

        $registros = $conexion->query($query);


        $output = '<ul class="list-unstyled" id="list_clientes">';
        if($registros->num_rows > 0)
        {
            while($row = $registros->fetch_assoc())
            {
                $output .= '<li class="ventas_list_clientes" value="'.$row["id"].'" >'.$row["nombre"].'</li>';
            }
        }
        else
        {
            $output .= '<li class="ventas_list_clientes" value="0">No hay coincidencias</li>';
        }
            $output .= '</ul>';
        return $output;
    }
    public function autocomplete_articulos($word){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');




        $query = "SELECT id,descripcion
                    FROM tb_articulos
                    where descripcion like '%".$word."%'
                    ";

        $registros = $conexion->query($query);


        $output = '<ul class="list-unstyled" id="list_articulos">';
        if($registros->num_rows > 0)
        {
            while($row = $registros->fetch_assoc())
            {
                $output .= '<li class="ventas_list_articulos" value="'.$row["id"].'" >'.$row["descripcion"].'</li>';
            }
        }
        else
        {
            $output .= '<li class="ventas_list_articulos" value="0">No hay coincidencias</li>';
        }
        $output .= '</ul>';
        return $output;
    }

    /*** GETS ***/
    public function get_rfc($userid){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');


        $query = "select rfc
                    from tb_clientes
                    where id = '$userid'
                    ";

        $registros = $conexion->query($query);

        $resultado='';
        if($registros->num_rows > 0)
        {
            if($row = $registros->fetch_assoc())
            {
                $resultado = $row['rfc'];
            }
        }
        else
        {
            $resultado = 'No hay coincidencias as';
        }

        return $resultado;
    }

    /*** GRID DE VENTA ***/
    public function grid_venta($venta,$articulo,$cantidad){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $mismo_articulo = $this->validar_articulo_grid($venta,$articulo);

        if($mismo_articulo == 1){
            $resultado['success'] = false;
            $resultado['message'] = 'Articulo ya agregado en el grid, favor de modificar la cantidad';
        }else{


            $this->store_detalle($venta,$articulo,$cantidad);
            $this->put_subtract_existencia_articulo($cantidad,$articulo);

            $query ="select tba.id,tba.descripcion,tba.modelo,tvd.cantidad,tvd.precio,tvd.importe,tvd.venta_id
                from tb_articulos tba
                inner join tb_ventas_detalle tvd on tvd.articulo_id = tba.id
                where tvd.venta_id = '$venta'
                    ";

            $registros = $conexion->query($query);

            $resultado=array();
            if($registros->num_rows > 0) {
                $resultado['success'] = true;
                while($row = $registros->fetch_assoc()){
                    $resultado['data'][] = $row;
                }
                $resultado['message'] = "Se encontraron registros.";

            }else{
                $resultado['success'] = false;
                $resultado['message'] = "No existen clientes";
            }
        }


        $enganche = $this->calcular_enganche($venta);
        $bonificacion = $this->calcular_bonificacion($enganche);
        $adeudo = $this->calcular_adeudo($this->obtener_importe($venta),$enganche,$bonificacion);

        $resultado['enganche'] = round($enganche,2);
        $resultado['bonificacion'] = round($bonificacion,2);
        $resultado['adeudo'] = round($adeudo,2);


        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;
    }
    public function grid_cantidad_articulo($venta,$articulo,$cantidad){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $back_stock = $this->obtener_cantidad_articulo_detalle($articulo);
        $this->put_add_existencia_articulo($back_stock ,$articulo);
        $stock_actual=$this->validar_existencia($articulo);
        $numero_stock_actual=$stock_actual->status;

        if($cantidad > $numero_stock_actual or $cantidad < 1){

            $this->update_detalle($venta,$articulo,1);
            $this->put_subtract_existencia_articulo(1,$articulo);

            $query ="select tba.id,tba.descripcion,tba.modelo,tvd.cantidad,tvd.precio,tvd.importe,tvd.venta_id
                from tb_articulos tba
                inner join tb_ventas_detalle tvd on tvd.articulo_id = tba.id
                where tvd.venta_id = '$venta'
                and tvd.articulo_id ='$articulo'
                    ";

            $registros = $conexion->query($query);

            $resultado=array();
            if($registros->num_rows > 0) {
                $resultado['success'] = false;
                if($row = $registros->fetch_assoc()){
                    $resultado['id'] = $row['id'];
                    $resultado['cantidad'] = $row['cantidad'];
                    $resultado['importe'] = $row['importe'];
                }
                $resultado['message'] = "Los datos ingresados no son correctos, favor de verificar";

            }else{
                $resultado['success'] = false;
                $resultado['message'] = "No existen clientes";
            }

        }else{

            $this->update_detalle($venta,$articulo,$cantidad);
            $this->put_subtract_existencia_articulo($cantidad,$articulo);

            $query ="select tba.id,tba.descripcion,tba.modelo,tvd.cantidad,tvd.precio,tvd.importe,tvd.venta_id
                from tb_articulos tba
                inner join tb_ventas_detalle tvd on tvd.articulo_id = tba.id
                where tvd.venta_id = '$venta'
                and tvd.articulo_id ='$articulo'
                    ";

            $registros = $conexion->query($query);

            $resultado=array();
            if($registros->num_rows > 0) {
                $resultado['success'] = true;
                if($row = $registros->fetch_assoc()){
                    $resultado['id'] = $row['id'];
                    $resultado['cantidad'] = $row['cantidad'];
                    $resultado['importe'] = $row['importe'];
                }
                $resultado['message'] = "Se encontraron registros.";

            }else{
                $resultado['success'] = false;
                $resultado['message'] = "No existen clientes";
            }



        }



        $enganche = $this->calcular_enganche($venta);
        $bonificacion = $this->calcular_bonificacion($enganche);
        $adeudo = $this->calcular_adeudo($this->obtener_importe($venta),$enganche,$bonificacion);

        $resultado['enganche'] = round($enganche,2);
        $resultado['bonificacion'] = round($bonificacion,2);
        $resultado['adeudo'] = round($adeudo,2);



        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;

    }
    public function delete_articulo_grid($venta,$articulo){
        $conexion= $this->conexion();

        $back_stock = $this->obtener_cantidad_articulo_detalle($articulo);
        $this->put_add_existencia_articulo($back_stock ,$articulo);


        $query_delete="DELETE FROM tb_ventas_detalle
                        where venta_id = '$venta'
                        and articulo_id = '$articulo' ";

        $conexion->query($query_delete);

        $query ="select tba.id,tba.descripcion,tba.modelo,tvd.cantidad,tvd.precio,tvd.importe,tvd.venta_id
                from tb_articulos tba
                inner join tb_ventas_detalle tvd on tvd.articulo_id = tba.id
                where tvd.venta_id = '$venta'";



        $registros = $conexion->query($query);

        $resultado=array();
        if($registros->num_rows > 0) {
            $resultado['success'] = true;
            if($row = $registros->fetch_assoc()){
                $resultado['id'] = $articulo;
                $resultado['cantidad'] = $row['cantidad'];
                $resultado['importe'] = $row['importe'];
            }
            $resultado['message'] = "Se encontraron registros.";
            $enganche = $this->calcular_enganche($venta);
            $bonificacion = $this->calcular_bonificacion($enganche);
            $adeudo = $this->calcular_adeudo($this->obtener_importe($venta),$enganche,$bonificacion);

            $resultado['enganche'] = round($enganche,2);
            $resultado['bonificacion'] = round($bonificacion,2);
            $resultado['adeudo'] = round($adeudo,2);

        }else{
            $resultado['success'] = false;
            $resultado['id'] = $articulo;
            $resultado['enganche'] = 0;
            $resultado['bonificacion'] = 0;
            $resultado['adeudo'] = 0;
            $resultado['message'] = "Todos los articulos han sido quitados, te recomendamos agregar un articulo.";
        }





        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;

    }

    public function get_abonos($venta){
        $this->store_deuda($venta);
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $query ="select total
                from tb_ventas_deuda
                where venta_id = '$venta'";


        $registros = $conexion->query($query);
        $resultado=array();
        if($registros->num_rows > 0) {
            $resultado['success'] = true;
            if($row = $registros->fetch_assoc()){
                $resultado['total'] = $row['total'];
            }
            $resultado['message'] = "Mensaje.";

        }else{
            $resultado['success'] = false;
            $resultado['message'] = "Mensaje";
        }

        $tasa = $this->get_configuracion_tasa();
        $plazo = $this->get_configuracion_plazo();

        $precio_contado = $resultado['total']/(1+(($tasa * $plazo)/100));

        $total_pagar_3 = $precio_contado*(1+($tasa*3)/100);
        $total_pagar_6 = $precio_contado*(1+($tasa*6)/100);
        $total_pagar_9 = $precio_contado*(1+($tasa*9)/100);
        $total_pagar_12 = $precio_contado*(1+($tasa*12)/100);

        $importe_abono_3 = $total_pagar_3/3;
        $importe_abono_6 = $total_pagar_6/6;
        $importe_abono_9 = $total_pagar_9/9;
        $importe_abono_12 = $total_pagar_12/12;

        $importe_ahorro_3 =$resultado['total']-$total_pagar_3;
        $importe_ahorro_6 =$resultado['total']-$total_pagar_6;
        $importe_ahorro_9 =$resultado['total']-$total_pagar_9;
        $importe_ahorro_12 =$resultado['total']-$total_pagar_12;

        $resultado['total_pagar_3']=round($total_pagar_3,2);
        $resultado['total_pagar_6']=round($total_pagar_6,2);
        $resultado['total_pagar_9']=round($total_pagar_9,2);
        $resultado['total_pagar_12']=round($total_pagar_12,2);

        $resultado['importe_abono_3']=round($importe_abono_3,2);
        $resultado['importe_abono_6']=round($importe_abono_6,2);
        $resultado['importe_abono_9']=round($importe_abono_9,2);
        $resultado['importe_abono_12']=round($importe_abono_12,2);

        $resultado['importe_ahorro_3']= round($importe_ahorro_3,2);
        $resultado['importe_ahorro_6']= round($importe_ahorro_6,2);
        $resultado['importe_ahorro_9']= round($importe_ahorro_9,2);
        $resultado['importe_ahorro_12']= round($importe_ahorro_12,2);



        $datos=json_decode(json_encode($resultado));
        return $datos;
    }
    public function store_venta($venta,$plazos,$cliente){

        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $query ="select total
                from tb_ventas_deuda
                where venta_id = '$venta'";


        $registros = $conexion->query($query);
        $resultado=array();
        if($registros->num_rows > 0) {
            if($row = $registros->fetch_assoc()){
                $resultado['total'] = $row['total'];
            }
        }

        $tasa = $this->get_configuracion_tasa();
        $plazo = $this->get_configuracion_plazo();

        $precio_contado = $resultado['total']/(1+(($tasa * $plazo)/100));
        $total_pagar = $precio_contado*(1+($tasa*$plazos)/100);
        $importe_abono = $total_pagar/$plazos;
        $importe_ahorro =$resultado['total']-$total_pagar;


        $query_insert="INSERT INTO
		                    tb_ventas(id,cliente_id,plazos,abonos,total,ahorro,fecha)
                       VALUES
                            ('$venta','$cliente','$plazos','$importe_abono','$total_pagar','$importe_ahorro',now())";


        if(!$conexion->query($query_insert)){
            $resultado['success'] = false;
            $resultado['message'] = 'asdasdas';
        }else{
            $resultado['success'] = true;
            $resultado['message'] = 'Bien Hecho. Tu venta ha sido registrada correctamente';
        }

        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;
    }
    public function cancel_venta($venta){

        $conexion= $this->conexion();


        $query ="select venta_id,articulo_id
                from tb_ventas_detalle
                where venta_id = '$venta'";



        $registros = $conexion->query($query);

        $resultado=array();
        if($registros->num_rows > 0) {
            $resultado['success'] = true;
            while($row = $registros->fetch_assoc()){
                $resultado['data'][] = $row;
            }
        }

        $query_delete="DELETE FROM tb_ventas_deuda
                        where venta_id = '$venta'";

        $conexion->query($query_delete);



        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;
    }

    /** DETALLE */
    public function store_detalle($venta,$articulo,$cantidad){
        $conexion= $this->conexion();
        $precio = $this->calcular_precio($articulo);
        $importe = $this->calcular_importe($precio,$cantidad);

        $query_insert="INSERT INTO
		                    tb_ventas_detalle(venta_id,articulo_id,precio,cantidad,importe,created_at)
                           VALUES
                            ('$venta','$articulo','$precio','$cantidad','$importe',now())";


        if(!$conexion->query($query_insert)){
            $resultado = false;
        }else{
            $resultado = true;
        }

        $conexion->close();
        return $resultado;
    }
    public function update_detalle($venta,$articulo,$cantidad){
        $conexion= $this->conexion();
        $precio = $this->calcular_precio($articulo);
        $importe = $this->calcular_importe($precio,$cantidad);

        $query_update ="UPDATE tb_ventas_detalle tvd
                SET tvd.cantidad = '$cantidad',tvd.importe='$importe'
                WHERE tvd.articulo_id='$articulo'
                AND tvd.venta_id ='$venta'
                LIMIT 1;";


        if(!$conexion->query($query_update)){
            $resultado = false;
        }else{
            $resultado = true;
        }

        $conexion->close();
        return $resultado;
    }


    /** STOCKS**/
    public function put_subtract_existencia_articulo($cantidad,$articulo){
        $conexion= $this->conexion();
        $existencia= $this->obtener_existencia_articulo($articulo);
        $upd_existencia = $existencia-$cantidad;

        $query_update ="UPDATE tb_articulos tba
                SET tba.existencia = '$upd_existencia'
                WHERE tba.id='$articulo'
                LIMIT 1;";

        if(!$conexion->query($query_update)){
            $resultado = false;
        }else{
            $resultado = true;
        }

        $conexion->close();
        return $resultado;
    }
    public function put_add_existencia_articulo($cantidad,$articulo){
        $conexion= $this->conexion();
        $existencia= $this->obtener_existencia_articulo($articulo);
        $upd_existencia = $existencia+$cantidad;

        $query_update ="UPDATE tb_articulos tba
                SET tba.existencia = '$upd_existencia'
                WHERE tba.id='$articulo'
                LIMIT 1;";

        if(!$conexion->query($query_update)){
            $resultado = false;
        }else{
            $resultado = true;
        }

        $conexion->close();
        return $resultado;
    }

    /*** DEUDAS ***/
    public function store_deuda($venta){
        $conexion= $this->conexion();
        $enganche = $this->calcular_enganche($venta);
        $bonificacion = $this->calcular_bonificacion($enganche);
        $adeudo = $this->calcular_adeudo($this->obtener_importe($venta),$enganche,$bonificacion);

        $query_insert="INSERT INTO
		                    tb_ventas_deuda(venta_id,enganche,bonificacion,total,created_at)
                       VALUES
                        ('$venta','$enganche','$bonificacion','$adeudo',now())";


        if(!$conexion->query($query_insert)){
            $resultado = false;
        }else{
            $resultado = true;
        }

        $conexion->close();
        return $resultado;
    }

    /** METODOS **/
    public function get_configuracion_tasa(){
        $conexion= $this->conexion();

        $query ="select tasa_financiamiento
                    from tb_configuraciones
                    ";
        $registros = $conexion->query($query);

        if($registros->num_rows > 0) {

            if($row = $registros->fetch_assoc()){
                $resultado = $row['tasa_financiamiento'];
            }

        }else{
            $resultado = 0;
        }

        $conexion->close();
        return $resultado;
    }
    public function get_configuracion_porcentaje_enganche(){
        $conexion= $this->conexion();

        $query ="select porcentaje_enganche
                    from tb_configuraciones
                    ";
        $registros = $conexion->query($query);

        if($registros->num_rows > 0) {

            if($row = $registros->fetch_assoc()){
                $resultado = $row['porcentaje_enganche'];
            }

        }else{
            $resultado = 0;
        }

        $conexion->close();
        return $resultado;
    }
    public function get_configuracion_plazo(){
        $conexion= $this->conexion();

        $query ="select plazo_maximo
                from tb_configuraciones
                    ";
        $registros = $conexion->query($query);

        if($registros->num_rows > 0) {

            if($row = $registros->fetch_assoc()){
                $resultado = $row['plazo_maximo'];
            }

        }else{
            $resultado = 0;
        }

        $conexion->close();
        return $resultado;
    }

    public function obtener_precio_articulo($articulo){
        $conexion= $this->conexion();

        $query ="select precio
                from tb_articulos
                where id = '$articulo'
                    ";
        $registros = $conexion->query($query);

        if($registros->num_rows > 0) {

            if($row = $registros->fetch_assoc()){
                $resultado = $row['precio'];
            }

        }else{
            $resultado = 0;
        }

        $conexion->close();
        return $resultado;
    }
    public function obtener_existencia_articulo($articulo){
        $conexion= $this->conexion();

        $query ="select existencia
                from tb_articulos
                where id = '$articulo'
                    ";
        $registros = $conexion->query($query);

        if($registros->num_rows > 0) {

            if($row = $registros->fetch_assoc()){
                $resultado = $row['existencia'];
            }

        }else{
            $resultado = 0;
        }

        $conexion->close();
        return $resultado;
    }

    public function obtener_cantidad_articulo_detalle($articulo){
        $conexion= $this->conexion();

        $query ="select cantidad
                from tb_ventas_detalle
                where articulo_id = '$articulo'
                    ";
        $registros = $conexion->query($query);

        if($registros->num_rows > 0) {

            if($row = $registros->fetch_assoc()){
                $resultado = $row['cantidad'];
            }

        }else{
            $resultado = 0;
        }

        $conexion->close();
        return $resultado;
    }

    public function calcular_precio($articulo){
        $tasa = $this->get_configuracion_tasa();
        $plazo = $this->get_configuracion_plazo();
        $precio_articulo = $this->obtener_precio_articulo($articulo);

        $precio = $precio_articulo*(1 +($tasa*$plazo/100));
        return $precio;
    }
    public function calcular_importe($precio,$cantidad){

        $importe = $precio*$cantidad;
        return $importe;
    }
    public function obtener_importe($venta){
        $conexion= $this->conexion();
        $importe=0;

        $query ="select sum(tvd.importe) as importe
                from tb_articulos tba
                inner join tb_ventas_detalle tvd on tvd.articulo_id = tba.id
                where tvd.venta_id = '$venta'";
        $registros = $conexion->query($query);

        if($registros->num_rows > 0) {

            if($row = $registros->fetch_assoc()){
                $importe = $row['importe'];
            }

        }

        $conexion->close();
        return $importe;
    }

    public function calcular_enganche($venta){

        $porcentaje_enganche = $this->get_configuracion_porcentaje_enganche();
        $importe =$this->obtener_importe($venta);
        $enganche = ($porcentaje_enganche/100)*$importe;
        return $enganche;
    }
    public function calcular_bonificacion($enganche){
        $tasa = $this->get_configuracion_tasa();
        $plazo = $this->get_configuracion_plazo();
        $bonificacion = $enganche*(($tasa*$plazo)/100);
        return $bonificacion;
    }
    public function calcular_adeudo($importe,$enganche,$bonificacion){
        $total_adeudo = $importe - $enganche-$bonificacion;
        return $total_adeudo;

    }


    /*** VALIDACIONES ROUTES ***/
    public function validar_cliente($cliente){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $query = "select count(id) as status
                    from tb_clientes
                    where id = $cliente";



        if($registros = $conexion->query($query)){

            if($row = $registros->fetch_assoc()){
                $resultado["success"] = true;
                $resultado["status"] = $row["status"];
            }else{
                $resultado['success'] = false;
                $resultado["status"] = 0;
            }

        }else{

            $resultado['success'] = false;
            $resultado['message'] = 'Favor de volver intentarlo';
            $resultado['error'] = $conexion-> error;
        }

        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;
    }
    public function validar_articulo($articulo){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $query = "select count(id) as status
                    from tb_articulos
                    where id = $articulo";



        if($registros = $conexion->query($query)){

            if($row = $registros->fetch_assoc()){
                $resultado["success"] = true;
                $resultado["status"] = $row["status"];
            }else{
                $resultado['success'] = false;
                $resultado["status"] = 0;
            }

        }else{

            $resultado['success'] = false;
            $resultado['message'] = 'Favor de volver intentarlo';
            $resultado['error'] = $conexion-> error;
        }

        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;
    }
    public function validar_existencia($articulo){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $query = "select existencia
                    from tb_articulos
                    where id = '$articulo'";



        if($registros = $conexion->query($query)){

            if($row = $registros->fetch_assoc()){
                $resultado["success"] = true;
                $resultado["status"] = $row["existencia"];
            }else{
                $resultado['success'] = false;
                $resultado['message'] = 'Favor de volver intentarlo';
            }

        }else{

            $resultado['success'] = false;
            $resultado['message'] = 'Favor de volver intentarlo';
            $resultado['error'] = $conexion-> error;
        }

        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;

    }
    public function validar_articulo_grid($venta,$articulo){
        $conexion= $this->conexion();

        $query ="select count(id) as status
                    from tb_ventas_detalle
                    where venta_id = '$venta'
                    and articulo_id = '$articulo'
                    ";
        $registros = $conexion->query($query);

        if($registros->num_rows > 0) {

            if($row = $registros->fetch_assoc()){
                $resultado = $row['status'];
            }

        }else{
            $resultado = 0;
        }

        $conexion->close();
        return $resultado;
    }
    public function validar_grid_con_articulos($venta){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $query = "select count(id) as status
                    from tb_ventas_detalle
                    where venta_id = '$venta'";



        if($registros = $conexion->query($query)){

            if($row = $registros->fetch_assoc()){
                $resultado["success"] = true;
                $resultado["status"] = $row["status"];
            }else{
                $resultado['success'] = false;
                $resultado["status"] = 0;
            }

        }else{

            $resultado['success'] = false;
            $resultado['message'] = 'Favor de volver intentarlo';
            $resultado['error'] = $conexion-> error;
        }

        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;
    }
}

?>
