<?php
require_once('connection.php');
ini_set('display_errors', 1);
error_reporting(E_ALL);

class Configuracion extends Conexion{


    public function store($tasa,$enganche,$plazo,$usuario){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $flag = $this->valid_configuracion();

        $option= $flag;


        if($option >= 1){
            $query ="UPDATE tb_configuraciones
                    SET tasa_financiamiento='$tasa',porcentaje_enganche='$enganche',plazo_maximo='$plazo',user_id=$usuario,updated_at=now()
                    where id = '1'
                    LIMIT 1";
        }else{


            $query="INSERT INTO
                    tb_configuraciones(tasa_financiamiento,porcentaje_enganche,plazo_maximo,user_id,created_at)
                VALUES
                    ('$tasa','$enganche','$plazo',$usuario,now())";
        }



        $resultado=array();
        if(!$conexion->query($query)){
            $resultado['success'] = false;
            $resultado['message'] = $conexion->error;
            $resultado['error'] = $conexion->error;

        }else{
            $resultado['success'] = true;
            $resultado['message'] = 'Bien hecho. La configuración ha sido registrada';

        }



        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;
    }

    public function show(){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $query ="select tasa_financiamiento,porcentaje_enganche,plazo_maximo
                    from tb_configuraciones
                    ";


        $registros = $conexion->query($query);

        $resultado=array();
        if($registros->num_rows > 0) {
            $resultado['success'] = true;
            if($row = $registros->fetch_assoc()){
                $resultado['data'][] = $row;
            }
            $resultado['message'] = "Se encontraron articulos.";



        }else{
            $resultado['success'] = false;
            $resultado['message'] = "No hay configuración";
        }




        $datos=json_decode(json_encode($resultado));
        $conexion->close();
        return $datos;
    }

    public function valid_configuracion(){
        $conexion= $this->conexion();
        mysqli_set_charset($conexion,'utf8');

        $query ="select count(id) as cond
                    from tb_configuraciones
                    ";


        $registros = $conexion->query($query);


        if($registros->num_rows > 0) {

            if($row = $registros->fetch_assoc()){
                $resultado = $row['cond'];
            }


        }else{
            $resultado=0;
        }


        $conexion->close();
        return $resultado;
    }
}

?>