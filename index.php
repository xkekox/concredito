<?php

session_start();
ob_start();
//print_r($_SESSION['session']);
if($_SESSION['user_id'] != null){
    //header("location: dashboard.php");
}else{
    header("location: login");
}

?>

<!doctype html>
<html lang="en">
<head>

    <?php include('includes/partials/styles.html') ?>
    <link rel="stylesheet" href="assets/css/styles.css"/>
</head>
<style>
    /* Let's get this party started */
    ::-webkit-scrollbar {
        width: 12px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        -webkit-border-radius: 10px;
        border-radius: 10px;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        -webkit-border-radius: 10px;
        border-radius: 10px;
        background: rgba(227,227,227,1);
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5);
    }
    ::-webkit-scrollbar-thumb:window-inactive {
        background: rgba(227,227,227,0.4);
    }

    .navbar-default .navbar-nav > li > a {
        color: white;
    }
</style>

<body style="background-color: white; padding-top: 70px; ">

    <!--  Navbar -->
    <?php include('includes/partials/menu.php'); ?>



    <!-- Contenido Princiapl -->
    <section>
        <div class="main_wrapper">

           <div class="container">


               <section>
                   <div class="container roboto">


                   </div>
               </section>






           </div>


        </div>
    </section>

    <input type="hidden" class="info_user" value="<?php echo $_SESSION['nombre'].' '.$_SESSION['apellidop'] ?>"/>

    <!-- Messages -->
    <section id="messages_inicio">
        <?php
        include('includes/messages/success.php');
        include('includes/messages/error.php');
        include('includes/messages/warning.php');
        ?>
    </section>

    <!-- Scripts Generales -->
    <section class="scripts">
        <?php include('includes/partials/scripts.php') ?>
    </section>


</body>
</html>