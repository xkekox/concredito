<?php
session_start();
session_destroy();
?>

<!DOCTYPE html>
<html >
<head>

  <?php include('includes/partials/styles.html') ?>
  <link rel="stylesheet" href="assets/css/login.css"/>

</head>

<body>
  <div class="login">
    <p align="center"><i class="fa fa-user-circle fa-inverse fa-5x" aria-hidden="true"></i></p>
    <img src="assets/img/icono.png" class="center-block" alt=""/>
	<h1 style="margin-top: 0px; color:#83b35b;" >ConCrédito</h1>
        <form method="post">
            <center><span class="text_center message_login text-success" style="font-size: 14px; margin-bottom: 10px; font-family: 'Roboto'" ></span></center>

            <input class="form-control" type="text" name="username" placeholder="Username" required="required" style="margin-bottom: 0px;"/>
            <span class="text_error_username text-danger" style="font-size: 12px;" ></span>
            <input class="form-control" type="password" name="password" placeholder="Password" required="required" style="margin-top: 10px;margin-bottom: 0px;"/>
            <span class="text_error_password text-danger" style="font-size: 12px;"></span>
            <button type="button" class="btn btn-success btn-block btn-large btn-login" style="margin-top: 10px; border-radius: 0px;">Entrar</button>
        </form>
    </div>


    <script src="assets/js/libs/jquery.js"></script>
    <script src="controllers/login.js"></script>

</body>
</html>
